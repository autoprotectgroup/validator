DROP CONSTRAINT ON (en:Entity) ASSERT en.path IS UNIQUE;
DROP CONSTRAINT ON (en:Field) ASSERT en.path IS UNIQUE;

DROP CONSTRAINT ON (en:Entity) ASSERT exists(en.name);
DROP CONSTRAINT ON (en:Entity) ASSERT exists(en.path);
DROP CONSTRAINT ON (en:Entity) ASSERT exists(en.type);
DROP CONSTRAINT ON (en:Entity) ASSERT exists(en.isCollection);
DROP CONSTRAINT ON (en:Entity) ASSERT exists(en.options);

DROP CONSTRAINT ON (en:Field) ASSERT exists(en.name);
DROP CONSTRAINT ON (en:Field) ASSERT exists(en.path);
DROP CONSTRAINT ON (en:Field) ASSERT exists(en.type);
DROP CONSTRAINT ON (en:Field) ASSERT exists(en.options);

DROP CONSTRAINT ON (en:Constraint) ASSERT exists(en.name);
DROP CONSTRAINT ON (en:Constraint) ASSERT exists(en.path);
DROP CONSTRAINT ON (en:Constraint) ASSERT exists(en.type);


CREATE CONSTRAINT ON (en:Field) ASSERT en.path IS UNIQUE;
CREATE CONSTRAINT ON (en:Entity) ASSERT en.path IS UNIQUE;

CREATE CONSTRAINT ON (en:Entity) ASSERT exists(en.name);
CREATE CONSTRAINT ON (en:Entity) ASSERT exists(en.path);
CREATE CONSTRAINT ON (en:Entity) ASSERT exists(en.type);
CREATE CONSTRAINT ON (en:Entity) ASSERT exists(en.isCollection);
CREATE CONSTRAINT ON (en:Entity) ASSERT exists(en.options);

CREATE CONSTRAINT ON (en:Field) ASSERT exists(en.name);
CREATE CONSTRAINT ON (en:Field) ASSERT exists(en.path);
CREATE CONSTRAINT ON (en:Field) ASSERT exists(en.type);
CREATE CONSTRAINT ON (en:Field) ASSERT exists(en.options);

CREATE CONSTRAINT ON (en:Constraint) ASSERT exists(en.name);
CREATE CONSTRAINT ON (en:Constraint) ASSERT exists(en.path);
CREATE CONSTRAINT ON (en:Constraint) ASSERT exists(en.type);



