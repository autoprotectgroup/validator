<?php

declare(strict_types=1);

namespace DealTrak\Validator\Validation\ExpressionFunction;

use ReflectionClass;
use Symfony\Component\ExpressionLanguage\ExpressionFunction;

abstract class AbstractValidationExpression extends ExpressionFunction
{
    public const EXPRESSION_NAME = '';

    public function __construct()
    {
        parent::__construct(
            static::getExpressionName(),
            $this->getCompiler(),
            $this->getEvaluator()
        );
    }

    public static function getExpressionName(): string
    {
        return !empty(static::EXPRESSION_NAME)
            ? static::EXPRESSION_NAME
            : lcfirst((new ReflectionClass(static::class))->getShortName());
    }
}
