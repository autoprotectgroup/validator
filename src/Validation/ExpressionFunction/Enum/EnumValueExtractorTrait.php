<?php

declare(strict_types=1);

namespace DealTrak\Validator\Validation\ExpressionFunction\Enum;

use BackedEnum;
use UnitEnum;

trait EnumValueExtractorTrait
{
    protected function getValueFromEnum(BackedEnum|UnitEnum $enum): string|int
    {
        if ($enum instanceof BackedEnum) {
            return $enum->value;
        }

        return $enum->name;
    }
}
