<?php

declare(strict_types=1);

namespace DealTrak\Validator\Validation\ExpressionFunction\Enum;

use BackedEnum;
use Closure;
use DealTrak\Validator\Validation\ExpressionFunction\AbstractValidationExpression;
use UnitEnum;

class EnumIn extends AbstractValidationExpression
{
    use EnumValueExtractorTrait;
    public const EXPRESSION_NAME = 'enumIn';

    public function getCompiler(): Closure
    {
        return static function (?string $statusCode): void {
            // not implemented yet. We use only evaluator there.
        };
    }

    public function getEvaluator(): Closure
    {
        return function (
            array $attributes,
            null|BackedEnum|UnitEnum $value,
            array $enumsStringValuesToSearchIn,
        ): bool {
            if (is_null($value)) {
                return false;
            }

            return in_array($this->getValueFromEnum($value), $enumsStringValuesToSearchIn, strict: true);
        };
    }
}
