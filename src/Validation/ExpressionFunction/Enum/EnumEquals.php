<?php

declare(strict_types=1);

namespace DealTrak\Validator\Validation\ExpressionFunction\Enum;

use BackedEnum;
use Closure;
use DealTrak\Validator\Validation\ExpressionFunction\AbstractValidationExpression;
use UnitEnum;

class EnumEquals extends AbstractValidationExpression
{
    use EnumValueExtractorTrait;
    public const EXPRESSION_NAME = 'enumEquals';

    public function getCompiler(): Closure
    {
        return static function (?string $statusCode): void {
            // not implemented yet. We use only evaluator there.
        };
    }

    public function getEvaluator(): Closure
    {
        return function (
            array $attributes,
            null|BackedEnum|UnitEnum $value,
            string|int $enumScalarValueToCompareWith,
        ): bool {
            if (is_null($value)) {
                return false;
            }

            return $this->getValueFromEnum($value) === $enumScalarValueToCompareWith;
        };
    }
}
