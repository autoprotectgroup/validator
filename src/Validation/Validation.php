<?php

declare(strict_types=1);

namespace DealTrak\Validator\Validation;

use ArrayIterator;
use BackedEnum;
use DealTrak\Validator\Validation\Error\ErrorCollection;
use DealTrak\Validator\Validation\Exception\ExpressionEvaluationException;
use DealTrak\Validator\Validation\Exception\NoMutatorFoundException;
use DealTrak\Validator\Validation\Rule\Model\GeneralRuleInterface;
use DealTrak\Validator\Validation\Rule\Types\ValidationRuleType;
use DealTrak\Validator\Validation\Rule\ValidationRuleInterface;
use DealTrak\Validator\Validation\Rule\ValidationRuleIterableInterface;
use DealTrak\Validator\Validation\Rule\ValidationScalarRule;
use Exception;
use InvalidArgumentException;
use RuntimeException;
use Symfony\Component\ExpressionLanguage\ExpressionLanguage;
use Traversable;
use UnitEnum;

/**
 * Class Validation
 *
 * @package DealTrak\Validator\Validation
 */
class Validation implements ValidationInterface
{
    private const DEFAULT_COUNT_ERRORS = 0;

    private array $totalFieldErrorsAmountByField = [];

    private bool $isRunValidate = false;

    protected ValidationRuleInterface $validationRule;
    protected ExpressionLanguage $expressionLanguage;

    /**
     * @param ValidationRuleInterface $validationRule
     *
     * @return $this
     */
    public function setValidationRules(ValidationRuleInterface $validationRule): self
    {
        $this->validationRule = $validationRule;

        return $this;
    }

    /**
     * @param ExpressionLanguage $expressionLanguage
     *
     * @return $this
     */
    public function setExpressionLanguage(ExpressionLanguage $expressionLanguage): self
    {
        $this->expressionLanguage = $expressionLanguage;

        return $this;
    }

    /**
     * @param object $value
     *
     * @param bool $validateAsEntity
     * @return iterable
     * @throws Exception
     */
    public function validate(object $value, bool $validateAsEntity = true): iterable
    {
        $this->isRunValidate = true;

        $validationRule = $this->getValidationRules();
        if ($validateAsEntity && $validationRule->getType() !== ValidationRuleType::TYPE_ENTITY) {
            $validationRule->setType(ValidationRuleType::TYPE_ENTITY);
            $validationRule->setIsModifiedInitialType(true);
        }

        return $this->collectErrors(
            $validationRule,
            $value,
            (new ErrorCollection())->setName(
                $this->getValidationRules()->getName()
            ),
            null,
            true
        );
    }

    public function getTotalFieldErrorsAmount(): int
    {
        return array_sum($this->totalFieldErrorsAmountByField);
    }

    public function getAmountOfErroneousFields(): int
    {
        return count(array_keys(array_filter($this->totalFieldErrorsAmountByField)));
    }

    /**
     * @return ValidationRuleInterface
     */
    public function getValidationRules(): ValidationRuleInterface
    {
        return $this->validationRule;
    }

    /**
     * @return bool
     */
    public function isValid(): bool
    {
        if (!$this->isRunValidate) {
            throw new InvalidArgumentException('Need to validate an object');
        }

        return $this->getTotalFieldErrorsAmount() === self::DEFAULT_COUNT_ERRORS;
    }

    /**
     * @param ValidationRuleInterface $validationRules
     * @param object|null $value
     * @param ErrorCollection $errors
     * @param object|null $parentObject
     * @param bool $isRootElement
     *
     * @return ErrorCollection
     * @throws Exception
     */
    protected function collectErrors(
        ValidationRuleInterface $validationRules,
        ?object $value,
        ErrorCollection $errors,
        ?object $parentObject = null,
        bool $isRootElement = false
    ): ErrorCollection {
        //Validate iterable type validation rule
        if ($validationRules instanceof ValidationRuleIterableInterface) {
            //Process entity validation rule
            if ($validationRules->getType() === ValidationRuleType::TYPE_ENTITY) {
                $errors = $this->processIterableRule($validationRules, $value, $errors, $parentObject);
                if (!is_null($value) && !is_object($value)) {
                    return $errors;
                }
            }

            //Process root collection validation rule
            if ($validationRules->getType() === ValidationRuleType::TYPE_COLLECTION && $isRootElement) {
                return $this->processIterableRule($validationRules, $value, $errors, $parentObject, $isRootElement);
            }
        }

        //Validate scalar type validation rule
        if ($validationRules instanceof ValidationScalarRule) {
            return $this->createErrorForScalar($validationRules, $value, $parentObject);
        }

        /** Check entity rules for empty values but not individual field rules */
        if (empty($value)) {
            return $errors;
        }

        /** @var ValidationRuleInterface $rule */
        foreach ($validationRules as $rule) {
            /**
             * - If the rule is not mutable, we pass it
             * - If the rule does not have mutable groups, we pass it
             * - If the filter group does not belong to the rule, we skip the rule
             * - If mutable group filter is empty, we skip the rule
             */

            $mutableGroupsFilter = $this->getMutableGroupsFilter($validationRules, $value);
            if ($rule->isMutable()
                && !empty($rule->getMutableGroups())
                && (empty($mutableGroupsFilter) ||
                    !in_array($mutableGroupsFilter, $rule->getMutableGroups(), true))
            ) {
                continue;
            }

            if ($rule instanceof ValidationRuleIterableInterface) {
                switch ($rule->getType()) {
                    case ValidationRuleType::TYPE_ENTITY:
                        $entityError = (new ErrorCollection())->setName($rule->getName());

                        $errors->addErrorCollection(
                            $this->collectErrors(
                                $rule,
                                $this->getObjectPropertyValue($rule->getName(), $value),
                                $entityError,
                                $value
                            )
                        );
                        continue 2;
                    case ValidationRuleType::TYPE_COLLECTION:
                        $errors->addErrorCollection($this->validateCollection($rule, $value, $parentObject));
                        continue 2;
                }
            }

            /** @var ValidationScalarRule $rule */
            if (!empty($rule->getExpression())) {
                $errors->addErrorCollection(
                    $this->createErrorForScalar($rule, $value, $parentObject)
                );
            }
        }

        return $errors;
    }

    /**
     * @param ValidationRuleInterface $validationRules
     * @param object|null $value
     * @return string|null
     */
    protected function getMutableGroupsFilter(ValidationRuleInterface $validationRules, ?object $value): ?string
    {
        if (!is_null($value) && $validationRules->isMutable()) {
            return $this->formatValue($this->getObjectPropertyValue(
                $validationRules->getMutableFieldOverride() ?? GeneralRuleInterface::DEFAULT_MUTABLE_FIELD,
                $value,
                true
            ));
        }
        return null;
    }

    /**
     * @param ValidationRuleInterface $validationRules
     * @param object|null $value
     * @param ErrorCollection $errors
     * @param object|null $parentObject
     *
     * @param bool $isRootElement
     * @return ErrorCollection
     * @throws Exception
     */
    protected function processIterableRule(
        ValidationRuleInterface $validationRules,
        ?object $value,
        ErrorCollection $errors,
        ?object $parentObject,
        bool $isRootElement = false
    ): ErrorCollection {
        $errors->setName($validationRules->getName());

        // Process validation of collection type rule
        if ($validationRules->getType() === ValidationRuleType::TYPE_COLLECTION) {
            return $errors
                ->setIsErrorRootElement($isRootElement)
                ->setIsCollectionField(true)
                ->addErrorCollection(
                    $this->validateCollection($validationRules, $value, $parentObject, $isRootElement)
                );
        } else {
            $errors->addFieldErrorMessage($validationRules->getErrorMessage());
            if (!is_null($value) && !is_object($value)) {
                return $errors;
            }

            return $this->validateEntity($validationRules, $value, $parentObject);
        }
    }

    /**
     * Process collection validation
     *
     * @param ValidationRuleInterface $rule
     * @param object $value
     * @param bool $isRootElement
     *
     * @param object|null $parentObject
     * @return ErrorCollection
     * @throws Exception
     */
    protected function validateCollection(
        ValidationRuleInterface $rule,
        object $value,
        ?object $parentObject = null,
        bool $isRootElement = false
    ): ErrorCollection {
        $collectionError = (new ErrorCollection())
            ->setName($rule->getName())
            ->setIsErrorRootElement($isRootElement)
            ->setIsCollectionField(true);

        if ($value instanceof Traversable) {
            $traversableValue = $value;
        } else {
            $parentObject = $value;
            $traversableValue = $this->getObjectPropertyValue($rule->getName(), $value);
            if (!is_array($traversableValue) && !$traversableValue instanceof Traversable) {
                return $this->createErrorForTraversable(
                    $collectionError,
                    $traversableValue
                );
            }
        }

        //In case collection expression exist then perform collection validation
        if (!empty($rule->getRules())) {
            $collectionError = $this->evaluateEntityOrCollectionExpression(
                $rule,
                $collectionError,
                new ArrayIterator($traversableValue),
                $parentObject,
            );
        }

        foreach ($traversableValue as $valueItemIndex => $valueItem) {
            $entityError = (new ErrorCollection())
                ->setName($rule->getName())
                ->setCollectionFieldIndex((int)$valueItemIndex);

            $rule->addExpressionVariable(
                $rule->getCollectionIndexName(),
                $valueItemIndex
            );

            $collectionError->addErrorCollection(
                $this->collectErrors($rule, $valueItem, $entityError, $parentObject)
            );
        }

        return $collectionError;
    }

    /**
     * Process entity validation
     *
     * @param ValidationRuleInterface $rule
     * @param object|null $value
     *
     * @param object|null $parentObject
     * @return ErrorCollection
     */
    protected function validateEntity(
        ValidationRuleInterface $rule,
        ?object $value,
        ?object $parentObject = null
    ): ErrorCollection {
        $entityError = (new ErrorCollection())
            ->setIsModifiedInitialType($rule->isModifiedInitialType())
            ->setIsEntityField(true)->setName($rule->getName());

        //If error on root element then enable IsErrorRootElement property
        if ($parentObject === null) {
            $entityError->setIsErrorRootElement(true);
        }


        //In case collection expression exist then perform entity validation
        if (!empty($rule->getRules())) {
            $entityError = $this->evaluateEntityOrCollectionExpression(
                $rule,
                $entityError,
                $value,
                $parentObject
            );
        }

        return $entityError;
    }

    /**
     * @param string $propertyName
     * @param object $objectToGetValueFrom
     * @param bool $throwException
     *
     * @return mixed
     */
    protected function getObjectPropertyValue(
        string $propertyName,
        object $objectToGetValueFrom,
        bool $throwException = false
    ): mixed {
        $propertyGetter = ucfirst($propertyName);

        $methodGetterName = 'get' . $propertyGetter;
        if (method_exists($objectToGetValueFrom, $methodGetterName)) {
            return $objectToGetValueFrom->$methodGetterName();
        }

        $methodGetterName = 'is' . $propertyGetter;
        if (method_exists($objectToGetValueFrom, $methodGetterName)) {
            return $objectToGetValueFrom->$methodGetterName();
        }

        if ($throwException) {
            throw new NoMutatorFoundException(
                sprintf(
                    'No getter found for the object %s and property %s',
                    get_class($objectToGetValueFrom),
                    $propertyName
                )
            );
        }

        return null;
    }

    private function formatValue(mixed $value): ?string
    {
        if ($value instanceof UnitEnum) {
            return $value instanceof BackedEnum ? $value->value : $value->name;
        }

        return $value !== null ? (string)$value : $value;
    }

    /**
     * @return ExpressionLanguage
     */
    protected function getExpressionLanguage(): ExpressionLanguage
    {
        if (!isset($this->expressionLanguage)) {
            $this->expressionLanguage = new ExpressionLanguage();
        }

        return $this->expressionLanguage;
    }

    /**
     * @param ValidationRuleInterface $validationRules
     * @param object $value
     * @param object|null $parentObject
     * @return ErrorCollection
     */
    protected function createErrorForScalar(
        ValidationRuleInterface $validationRules,
        object $value,
        ?object $parentObject = null
    ): ErrorCollection {
        $currentErrorCollection = (new ErrorCollection())
            ->setName($validationRules->getName());

        $currentValue = $this->getObjectPropertyValue($validationRules->getName(), $value);

        /** @var GeneralRuleInterface $constraint */
        foreach ($validationRules->getRules() as $constraint) {
            if (!isset($this->totalFieldErrorsAmountByField[$validationRules->getPath()])) {
                $this->totalFieldErrorsAmountByField[$validationRules->getPath()] = 0;
            }

            try {
                if (!$this->getExpressionLanguage()->evaluate(
                    $constraint->getExpression(),
                    array_merge(
                        $validationRules->getExpressionVariables(),
                        [
                            'value' => $currentValue,
                            'this' => $value,
                            'parent' => $parentObject,
                        ]
                    )
                )) {
                    $currentErrorCollection->addFieldErrorMessage($constraint->getErrorMessage());

                    $this->totalFieldErrorsAmountByField[$validationRules->getPath()]++;
                }
            } catch (RuntimeException $runtimeException) {
                $currentErrorCollection->addFieldErrorMessage($runtimeException->getMessage());

                $this->totalFieldErrorsAmountByField[$validationRules->getPath()]++;
            }
        }

        return $currentErrorCollection;
    }

    /**
     * @param ValidationRuleInterface $rule
     * @param ErrorCollection $errors
     *
     * @param object|null $value
     * @param object|null $parentObject
     * @return ErrorCollection
     */
    protected function evaluateEntityOrCollectionExpression(
        ValidationRuleInterface $rule,
        ErrorCollection $errors,
        ?object $value,
        ?object $parentObject = null
    ): ErrorCollection {
        if (!isset($this->totalFieldErrorsAmountByField[$rule->getPath()])) {
            $this->totalFieldErrorsAmountByField[$rule->getPath()] = 0;
        }

        $processedRules = [];
        /** @var GeneralRuleInterface $constraint */
        foreach ($rule->getRules() as $constraint) {
            //Check if this rule was already processed
            //In order not to trigger similar validation and prevent returning duplicate error messages
            if (in_array($constraint, $processedRules)) {
                continue;
            }
            array_push($processedRules, $constraint);

            if (empty($constraint->getExpression())) {
                continue;
            }

            if ($value instanceof ArrayIterator && $constraint->getType() !== ValidationRuleType::TYPE_COLLECTION) {
                foreach ($value as $itemIndex => $entity) {
                    $errors = $this->processEntityOrCollectionValidation(
                        $rule,
                        $constraint,
                        $errors,
                        $entity,
                        $parentObject,
                        $itemIndex
                    );
                }
            } else {
                $errors = $this->processEntityOrCollectionValidation(
                    $rule,
                    $constraint,
                    $errors,
                    $value,
                    $parentObject
                );
            }
        }

        return $errors;
    }

    /**
     * @param ValidationRuleInterface $rule
     * @param GeneralRuleInterface $constraint
     * @param ErrorCollection $errors
     * @param object|null $value
     * @param object|null $parentObject
     * @param int|null $index
     * @return ErrorCollection
     */
    protected function processEntityOrCollectionValidation(
        ValidationRuleInterface $rule,
        GeneralRuleInterface $constraint,
        ErrorCollection $errors,
        ?object $value,
        ?object $parentObject = null,
        ?int $index = null
    ): ErrorCollection {
        try {
            if (!$this->getExpressionLanguage()->evaluate(
                $constraint->getExpression(),
                array_merge(
                    $rule->getExpressionVariables(),
                    [
                        'value' => $value,
                        'parent' => $parentObject,
                    ]
                )
            )) {
                $this->totalFieldErrorsAmountByField[$rule->getPath()]++;
                $errors->addFieldErrorMessage($constraint->getErrorMessage(), $index);
            }
        } catch (Exception $exception) {
            throw new ExpressionEvaluationException($exception->getMessage());
        }

        return $errors;
    }

    /**
     * @param ErrorCollection $errors
     * @param array|object $value
     *
     * @return ErrorCollection
     */
    protected function createErrorForTraversable(
        ErrorCollection $errors,
        $value
    ): ErrorCollection {
        return $errors
            ->addFieldErrorMessage(
                sprintf(
                    'The field %s must be an array or Traversable. It is of type %s instead',
                    $errors->getName(),
                    gettype($value)
                )
            );
    }
}
