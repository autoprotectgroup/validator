<?php

declare(strict_types=1);

namespace DealTrak\Validator\Validation\Exception;

use RuntimeException;

class InvalidRuleTypeException extends RuntimeException
{
}
