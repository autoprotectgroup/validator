<?php

declare(strict_types=1);

namespace DealTrak\Validator\Validation\Exception;

use RuntimeException;

class ExpressionEvaluationException extends RuntimeException
{

}
