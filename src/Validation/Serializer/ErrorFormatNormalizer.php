<?php

declare(strict_types=1);

namespace DealTrak\Validator\Validation\Serializer;

class ErrorFormatNormalizer
{
    protected const ERROR_TYPE = 'validation';

    public function normalize(array $errors, string $modelType = ''): array
    {
        $normalizedError = [
            'errors' => [
                'type' => self::ERROR_TYPE
            ]
        ];
        $normalizedError['errors']['detail'] = empty($modelType) ? $errors : [$modelType => $errors];

        return $normalizedError;
    }
}
