<?php

declare(strict_types=1);

namespace DealTrak\Validator\Validation\Serializer;

use DealTrak\Validator\Validation\Error\ErrorCollection;
use Throwable;

class ErrorCollectionSerializer
{
    protected const ITEM_ORDER = 'listItemOrder';
    protected const ERRORS_PROPERTY = 'errors';
    protected const FIELDS_PROPERTY = 'fields';
    protected const ITEMS_PROPERTY = 'items';

    /**
     * @param ErrorCollection $errorCollection
     *
     * @return array
     */
    public function serialize(ErrorCollection $errorCollection): array
    {
        $errors = $this->isSetInitialRootStructure($errorCollection) ?
            $this->setInitialRootErrorStructure($errorCollection) : [];

        /** @var ErrorCollection $error */
        foreach ($errorCollection as $error) {
            $combinedErrors = $error->hasCollectionErrors()
                ? $this->serialize($error)
                : $error->getFieldErrorMessages();

            if ($error->isCollectionField()) {
                //Check if root elem of collection has error and add error in array
                try {
                    $error->getCollectionFieldIndex();
                } catch (Throwable) {
                    //In case only collection error without nested errors than build collection item error structure
                    if ($error->getFieldErrorMessages() && !isset($combinedErrors[$error->getName()])) {
                        $errors[self::FIELDS_PROPERTY][$error->getName()] =
                            $this->isOnlyCollectionErrors($combinedErrors) ?
                                $this->processCollectionErrorFormat($combinedErrors) :
                                [
                                    self::ERRORS_PROPERTY => $combinedErrors,
                                    self::ITEMS_PROPERTY => []
                                ];
                    }
                }
                /** @var ErrorCollection $collectionErrorItem */
                foreach ($error as $itemIndex => $collectionErrorItem) {
                    try {
                        $order = $collectionErrorItem->getCollectionFieldIndex();
                    } catch (Throwable) {
                        $order = $itemIndex;
                    }

                    $nestedErrors = $this->serialize($collectionErrorItem);
                    $entityError = $this->getCollectionEntityErrorsByIndex($error->getFieldErrorMessages(), $order);
                    //Check if has fields or entity errors
                    if (!empty($nestedErrors) || !empty($entityError)) {
                        // In case no fields error then set initial field error structure
                        if (!isset($nestedErrors[self::FIELDS_PROPERTY])) {
                            $nestedErrors = [self::FIELDS_PROPERTY => []];
                        }
                        //Set collection item error structure
                        $nestedErrorsWithOrder = array_merge(
                            $nestedErrors,
                            [
                                self::ITEM_ORDER => $itemIndex,
                                self::ERRORS_PROPERTY => $entityError

                            ]
                        );

                        //Build collection error structure
                        $errors = $this->buildCollectionErrorStructure($errors, $error, $nestedErrorsWithOrder);
                    }
                }
            } elseif ($error->isEntityField()) {
                //Set root element error structure for the form validation type
                if (count($combinedErrors) > 0 || count($error->getFieldErrorMessages()) > 0) {
                    $errors[self::FIELDS_PROPERTY][$error->getName()] =
                        $this->buildEntityErrorStructure($error, $combinedErrors);
                }
            } else {
                if (count($combinedErrors) > 0) {
                    if (isset($errors[self::FIELDS_PROPERTY][$error->getName()])) {
                        array_push($errors[self::FIELDS_PROPERTY][$error->getName()], current($combinedErrors));
                    } else {
                        $errors[self::FIELDS_PROPERTY][$error->getName()] = $combinedErrors;
                    }
                }
            }
        }

        //In case processed nested error than return current loop error structure
        if (!$errorCollection->isErrorRootElement()) {
            return $errors;
        }

        return $this->buildProcessedErrorStructure($errorCollection, $errors);
    }

    /**
     * Currently in validator if root element is a collection then we process such rule as entity
     * In this method we check is initial type of rule is entity and if it was not modified
     *
     * @param ErrorCollection $errorCollection
     * @return bool
     */
    private function isRootElementEntity(ErrorCollection $errorCollection): bool
    {
        return $errorCollection->isEntityField() && !$errorCollection->isModifiedInitialType();
    }

    /**
     * @param ErrorCollection $errorCollection
     * @return string
     */
    private function getStructurePropertyName(ErrorCollection $errorCollection): string
    {
        return $this->isRootElementEntity($errorCollection) ? self::FIELDS_PROPERTY : self::ITEMS_PROPERTY;
    }

    /**
     * @param array $errors
     * @param int $index
     * @return array
     */
    private function getCollectionEntityErrorsByIndex(array $errors, int $index): array
    {
        if (!isset($errors[ErrorCollection::ENTITY_ERRORS])) {
            return [];
        }

        return $errors[ErrorCollection::ENTITY_ERRORS][$index] ?? [];
    }

    /**
     * @param ErrorCollection $errorCollection
     * @return bool
     */
    private function isSetInitialRootStructure(ErrorCollection $errorCollection): bool
    {
        return $errorCollection->isErrorRootElement() || $errorCollection->isCollectionField();
    }

    /**
     * @param array $errors
     * @return array
     */
    private function processCollectionErrorFormat(array $errors): array
    {
        if (isset($errors[self::ERRORS_PROPERTY][ErrorCollection::ENTITY_ERRORS])) {
            $entityErrors = $errors[self::ERRORS_PROPERTY][ErrorCollection::ENTITY_ERRORS];

            unset($errors[self::ERRORS_PROPERTY][ErrorCollection::ENTITY_ERRORS]);
            foreach ($entityErrors as $index => $error) {
                $errors[self::ITEMS_PROPERTY][] = [
                    self::FIELDS_PROPERTY => [],
                    self::ITEM_ORDER => $index,
                    self::ERRORS_PROPERTY => $error,
                ];
            }
        }
        return $errors;
    }

    /**
     * @param ErrorCollection $errorCollection
     * @return array
     */
    private function setInitialRootErrorStructure(ErrorCollection $errorCollection): array
    {
        return [
            ($this->getStructurePropertyName($errorCollection)) => [],
            self::ERRORS_PROPERTY => $errorCollection->getFieldErrorMessages(),
        ];
    }


    /**
     * @param array $errors
     * @param ErrorCollection $error
     * @return array
     */
    private function filterCollectionErrorProperties(array $errors, ErrorCollection $error): array
    {
        //Remove fields that used in error response serialization but unnecessary in response
        if (!empty($collectionErrors = $errors[self::FIELDS_PROPERTY][$error->getName()] ?? [])) {
            unset($collectionErrors[self::FIELDS_PROPERTY]);
            unset($collectionErrors[self::ERRORS_PROPERTY][ErrorCollection::ENTITY_ERRORS]);
            $errors[self::FIELDS_PROPERTY][$error->getName()] = $collectionErrors;
        }

        return $errors;
    }


    /**
     * Prepare final error structure after processing all nested errors
     *
     * @param ErrorCollection $errorCollection
     * @param array $errors
     * @return array
     */
    private function buildProcessedErrorStructure(ErrorCollection $errorCollection, array $errors): array
    {
        //Check if root element is entity then return error structure
        if ($errorCollection->isEntityField()) {
            unset($errors[self::ITEMS_PROPERTY]);
            if (!isset($errors[self::FIELDS_PROPERTY])) {
                $errors[self::FIELDS_PROPERTY] = [];
            }
            return $errors;
        }

        return $errors[self::FIELDS_PROPERTY] ?? $errors;
    }

    /**
     * @param ErrorCollection $error
     * @param array $entityError
     * @return array
     */
    private function buildEntityErrorStructure(ErrorCollection $error, array $entityError): array
    {
        //Set if needed required 'fields' property
        if (!isset($entityError[self::FIELDS_PROPERTY])) {
            $entityError = [self::FIELDS_PROPERTY => []];
        }
        $entityError[self::ERRORS_PROPERTY] = $error->getFieldErrorMessages();

        return $entityError;
    }

    /**
     * @param array $errors
     * @param ErrorCollection $error
     * @param array $nestedErrors
     * @return array
     */
    private function buildCollectionErrorStructure(array $errors, ErrorCollection $error, array $nestedErrors): array
    {
        //Remove property name from the errors structure
        if (count($nestedErrors) > 0) {
            $errors = $this->filterCollectionItemsStructure($errors, $error, $nestedErrors);
            $errors[self::FIELDS_PROPERTY][$error->getName()][self::ITEMS_PROPERTY][] = $nestedErrors;
        }
        $errors[self::FIELDS_PROPERTY][$error->getName()][self::ERRORS_PROPERTY] =
            $error->getFieldErrorMessages();

        return $this->filterCollectionErrorProperties($errors, $error);
    }

    /**
     * Filter collection items in case they were already initialized in the root structure
     *
     * @param array $errors
     * @param ErrorCollection $error
     * @param array $nestedErrors
     * @return array
     */
    private function filterCollectionItemsStructure(
        array $errors,
        ErrorCollection $error,
        array $nestedErrors
    ): array {
        if (isset($errors[self::FIELDS_PROPERTY][$error->getName()][self::ITEMS_PROPERTY])) {
            $arrItems = $errors[self::FIELDS_PROPERTY][$error->getName()][self::ITEMS_PROPERTY];
            $errors[self::FIELDS_PROPERTY][$error->getName()][self::ITEMS_PROPERTY] =
                array_filter($arrItems, function ($item) use ($nestedErrors) {
                    return $item[self::ITEM_ORDER] !== $nestedErrors[self::ITEM_ORDER];
                });
        }
        return $errors;
    }

    /**
     * Check if array is associative and don't have any numeric keys
     *
     * @param array $errors
     * @return bool
     */
    private function isOnlyCollectionErrors(array $errors): bool
    {
        if (array() === $errors) {
            return false;
        }
        return array_keys($errors) !== range(0, count($errors) - 1);
    }
}
