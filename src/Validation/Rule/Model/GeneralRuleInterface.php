<?php

declare(strict_types=1);

namespace DealTrak\Validator\Validation\Rule\Model;

interface GeneralRuleInterface
{
    public const DEFAULT_MUTABLE_FIELD = 'type';

    /**
     * Get aggregated expression name (fieldName in general)
     *
     * @return string
     */
    public function getName(): string;

    /**
     * Get expression path
     *
     * @return string
     */
    public function getPath(): string;

    /**
     * Check that expression is mutable. Rule is based on an entity that can change validation rules depending
     * on the type provided by the entity, like an Asset can be a Car or a Boat and can change the validation rules set.
     * The type of the entity defines the group to which the scalar mutable rule should belong to.
     *
     * @return bool
     */
    public function isMutable(): bool;

    /**
     * Get mutable groups array
     *
     * @return array
     */
    public function getMutableGroups(): array;

    /**
     * An override for a default mutable field identifier.
     * By default mutable group is defined by `type` field. If we want to get the mutable group
     * from another field, we can do so by providing this value via `getMutableFieldOverride`
     *
     * @return string|null
     */
    public function getMutableFieldOverride(): ?string;

    /**
     * Get fundamental field type (Text, Number, ...)
     *
     * @return string
     */
    public function getType(): string;

    /**
     * Get expression itself
     *
     * @return string
     */
    public function getExpression(): string;

    /**
     * Class name to validate the data against. This is optional and can be omitted
     *
     * @return string|null
     */
    public function getClassName(): ?string;

    /**
     * Get the violation message for the validation rule
     *
     * @return string
     */
    public function getErrorMessage(): ?string;

    /**
     * If the rule is collection, we need to store the index variable name
     *
     * @return string|null
     */
    public function getCollectionIndexName(): ?string;
}
