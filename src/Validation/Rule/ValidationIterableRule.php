<?php

declare(strict_types=1);

namespace DealTrak\Validator\Validation\Rule;

use ArrayIterator;
use Exception;
use Traversable;

/**
 * Class ValidationIterableRule
 *
 * @package DealTrak\Validator\Rule\Provider
 */
class ValidationIterableRule extends AbstractValidationRule implements ValidationRuleIterableInterface
{
    private iterable $validationExpressionRules = [];

    /**
     * @param iterable[] $expressions
     *
     * @return ValidationRuleIterableInterface
     */
    public function setExpressions(iterable $expressions): ValidationRuleIterableInterface
    {
        $this->validationExpressionRules = $expressions;
        return $this;
    }

    /**
     * @param ValidationRuleInterface $rule
     *
     * @return $this
     */
    public function addExpression(ValidationRuleInterface $rule): self
    {
        $this->validationExpressionRules[] = $rule;
        return $this;
    }

    /**
     * {@inheritDoc}
     */
    public function getIterator(): Traversable
    {
        return new ArrayIterator($this->validationExpressionRules);
    }

    /**
     * Add expression variable
     *
     * @param string $name
     * @param        $value
     * @param bool   $setForChildRules
     *
     * @return $this
     * @throws Exception
     */
    public function addExpressionVariable(string $name, $value, bool $setForChildRules = true): self
    {
        $this->expressionVariables[$name] = $value;

        if ($setForChildRules) {
            /** @var ValidationIterableRule $rule */
            foreach ($this->getIterator() as $rule) {
                $rule->addExpressionVariable($name, $value, true);
            }
        }
        return $this;
    }
}
