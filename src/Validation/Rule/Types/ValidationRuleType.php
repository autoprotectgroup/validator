<?php

declare(strict_types=1);

namespace DealTrak\Validator\Validation\Rule\Types;

class ValidationRuleType
{
    public const TYPE_FIELD = 'field';
    public const TYPE_ENTITY = 'entity';
    public const TYPE_COLLECTION = 'collection';
}
