<?php

declare(strict_types=1);

namespace DealTrak\Validator\Validation\Rule;

use DealTrak\Validator\Validation\Rule\Model\GeneralRuleInterface;

/**
 * Class AbstractValidationRule
 *
 * @package DealTrak\Validator\Rule\Provider
 */
abstract class AbstractValidationRule implements ValidationRuleInterface
{
    protected string $name;
    protected string $path;
    protected string $expression;
    protected string $type;
    protected array $options = [];
    protected bool $isMutable = false;
    protected bool $isModifiedInitialType = false;
    protected array $mutableGroups = [];
    protected ?string $mutableFieldOverride = null;
    protected array $ruleExpressionConstraints = [];
    protected ?string $className = null;
    protected array $expressionVariables = [];
    protected string $errorMessage = '';
    protected string $collectionIndexName = '';

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @return string
     */
    public function getPath(): string
    {
        return $this->path;
    }

    /**
     * @param string $path
     *
     * @return self
     */
    public function setPath(string $path): self
    {
        $this->path = $path;
        return $this;
    }

    /**
     * @return string
     */
    public function getExpression(): string
    {
        return $this->expression;
    }

    /**
     * @param string $expression
     *
     * @return ValidationRuleInterface
     */
    public function setExpression(string $expression): ValidationRuleInterface
    {
        $this->expression = $expression;

        return $this;
    }

    /**
     * @param string $name
     *
     * @return $this
     */
    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    /**
     * @return string
     */
    public function getType(): string
    {
        return $this->type;
    }

    /**
     * @param string $type
     *
     * @return $this
     */
    public function setType(string $type): self
    {
        $this->type = $type;

        return $this;
    }

    /**
     * @return array
     */
    public function getOptions(): array
    {
        return $this->options;
    }

    /**
     * @param array $options
     *
     * @return $this
     */
    public function setOptions(array $options): self
    {
        $this->options = $options;

        return $this;
    }

    /**
     * @return bool
     */
    public function isMutable(): bool
    {
        return $this->isMutable;
    }

    /**
     * @param bool $isMutable
     * @return self
     */
    public function setIsMutable(bool $isMutable): self
    {
        $this->isMutable = $isMutable;

        return $this;
    }

    /**
     * @return bool
     */
    public function isModifiedInitialType(): bool
    {
        return $this->isModifiedInitialType;
    }

    /**
     * @param bool $isModifiedInitialType
     * @return self
     */
    public function setIsModifiedInitialType(bool $isModifiedInitialType): self
    {
        $this->isModifiedInitialType = $isModifiedInitialType;

        return $this;
    }

    /**
     * @return array
     */
    public function getMutableGroups(): array
    {
        return $this->mutableGroups;
    }

    /**
     * @param array $mutableGroups
     * @return self
     */
    public function setMutableGroups(array $mutableGroups): self
    {
        $this->mutableGroups = $mutableGroups;

        return $this;
    }

    /**
     * @return string|null
     */
    public function getMutableFieldOverride(): ?string
    {
        return $this->mutableFieldOverride;
    }

    /**
     * @param string $mutableFieldOverride
     * @return $this
     */
    public function setMutableFieldOverride(?string $mutableFieldOverride): self
    {
        $this->mutableFieldOverride = $mutableFieldOverride;

        return $this;
    }

    /**
     * {@inheritDoc}
     */
    public function getClassName(): ?string
    {
        return $this->className;
    }

    /**
     * @param string $className
     *
     * @return self
     */
    public function setClassName(string $className): self
    {
        $this->className = $className;
        return $this;
    }

    /**
     * {@inheritDoc}
     */
    public function getRules(): iterable
    {
        return $this->ruleExpressionConstraints;
    }

    public function addExpressionConstraintRule(GeneralRuleInterface $rule): self
    {
        $this->ruleExpressionConstraints[] = $rule;
        return $this;
    }

    public function setExpressionVariables(array $globalExpressionVariables): self
    {
        $this->expressionVariables = $globalExpressionVariables;
        return $this;
    }

    /**
     * @return array
     */
    public function getExpressionVariables(): array
    {
        return $this->expressionVariables;
    }

    /**
     * {@inheritDoc}
     */
    public function getErrorMessage(): ?string
    {
        return $this->errorMessage;
    }

    /**
     * @param string $errorMessage
     *
     * @return self
     */
    public function setErrorMessage(string $errorMessage): self
    {
        $this->errorMessage = $errorMessage;

        return $this;
    }

    public function getCollectionIndexName(): ?string
    {
        return $this->collectionIndexName;
    }

    public function setCollectionIndexName(?string $collectionIndexName): self
    {
        $this->collectionIndexName = $collectionIndexName;
        return $this;
    }
}
