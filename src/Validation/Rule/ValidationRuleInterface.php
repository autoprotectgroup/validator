<?php

declare(strict_types=1);

namespace DealTrak\Validator\Validation\Rule;

use DealTrak\Validator\Validation\Rule\Model\GeneralRuleInterface;

/**
 * Interface ValidationRuleInterface
 *
 * @package DealTrak\Validator\Rule\Provider
 */
interface ValidationRuleInterface extends GeneralRuleInterface
{
    /**
     * Get expression rules of the field or entity or collection
     *
     * @return iterable
     */
    public function getRules(): iterable;

    /**
     * Set global expression variables for the symfony expressions
     *
     * @param array $globalExpressionVariables
     *
     * @return $this
     */
    public function setExpressionVariables(array $globalExpressionVariables): self;
}
