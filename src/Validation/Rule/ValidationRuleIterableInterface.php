<?php

declare(strict_types=1);

namespace DealTrak\Validator\Validation\Rule;

use \IteratorAggregate;

/**
 * Interface ValidationRuleIterableInterface
 *
 * @package DealTrak\Validator\Rule\Provider
 */
interface ValidationRuleIterableInterface extends IteratorAggregate
{

}
