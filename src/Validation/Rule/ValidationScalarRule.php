<?php

declare(strict_types=1);

namespace DealTrak\Validator\Validation\Rule;

use Exception;

/**
 * Class ValidationScalarRule
 *
 * @package DealTrak\Validator\Rule\Provider
 */
class ValidationScalarRule extends AbstractValidationRule
{
    /**
     * Add expression variable
     *
     * @param string $name
     * @param        $value
     *
     * @return $this
     * @throws Exception
     */
    public function addExpressionVariable(string $name, $value): self
    {
        $this->expressionVariables[$name] = $value;

        return $this;
    }
}
