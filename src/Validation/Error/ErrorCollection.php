<?php

declare(strict_types=1);

namespace DealTrak\Validator\Validation\Error;

use ArrayIterator;
use IteratorAggregate;

/**
 * Class ErrorCollection
 *
 * @package DealTrak\Validator\Validation\Error
 */
class ErrorCollection implements IteratorAggregate, ErrorCollectionInterface
{
    public const ENTITY_ERRORS = 'entityErrors';

    protected string $name;
    protected bool $isCollectionField = false;
    protected bool $isErrorRootElement = false;
    protected bool $isEntityField = false;
    protected bool $isModifiedInitialType = false;
    protected int $collectionFieldIndex;
    protected array $errors = [];
    protected array $fieldErrorMessages = [];

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * {@inheritDoc}
     */
    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    /**
     * @return bool
     */
    public function isCollectionField(): bool
    {
        return $this->isCollectionField;
    }

    /**
     * @param bool $isCollectionField
     *
     * @return ErrorCollection
     */
    public function setIsCollectionField(bool $isCollectionField): self
    {
        $this->isCollectionField = $isCollectionField;

        return $this;
    }

    /**
     * @return bool
     */
    public function isErrorRootElement(): bool
    {
        return $this->isErrorRootElement;
    }

    /**
     * @param bool $isErrorRootElement
     *
     * @return ErrorCollection
     */
    public function setIsErrorRootElement(bool $isErrorRootElement): self
    {
        $this->isErrorRootElement = $isErrorRootElement;

        return $this;
    }

    /**
     * @return bool
     */
    public function isModifiedInitialType(): bool
    {
        return $this->isModifiedInitialType;
    }

    /**
     * @param bool $isModifiedInitialType
     * @return self
     */
    public function setIsModifiedInitialType(bool $isModifiedInitialType): self
    {
        $this->isModifiedInitialType = $isModifiedInitialType;

        return $this;
    }

    /**
     * @return bool
     */
    public function isEntityField(): bool
    {
        return $this->isEntityField;
    }

    /**
     * @param bool $isEntityField
     *
     * @return ErrorCollection
     */
    public function setIsEntityField(bool $isEntityField): self
    {
        $this->isEntityField = $isEntityField;

        return $this;
    }

    /**
     * @return int
     */
    public function getCollectionFieldIndex(): int
    {
        return $this->collectionFieldIndex;
    }

    /**
     * @param int $collectionFieldIndex
     *
     * @return $this
     */
    public function setCollectionFieldIndex(int $collectionFieldIndex): self
    {
        $this->collectionFieldIndex = $collectionFieldIndex;

        return $this;
    }

    /**
     * @return bool
     */
    public function hasCollectionErrors(): bool
    {
        return (bool)count($this->errors);
    }

    /**
     * @return bool
     */
    public function hasFieldErrorMessages(): bool
    {
        return (bool)count($this->fieldErrorMessages);
    }

    /**
     *{@inheritDoc}
     */
    public function addErrorCollection(ErrorCollectionInterface $violationCollection): self
    {
        $this->errors[] = $violationCollection;

        return $this;
    }

    /**
     * {@inheritDoc}
     */
    public function getErrorCollection(): array
    {
        return $this->errors;
    }

    /**
     * {@inheritDoc}
     */
    public function addFieldErrorMessage(string $message, ?int $index = null): self
    {
        //If index provided then we process this error message as collection's entity error
        //And put in collection's level messages based on the entity item index
        ($index !== null) ?
            $this->fieldErrorMessages[self::ENTITY_ERRORS][$index][] = $message :
            $this->fieldErrorMessages[] = $message;

        return $this;
    }

    /**
     * @return array
     */
    public function getFieldErrorMessages(): array
    {
        return $this->fieldErrorMessages;
    }

    /**
     * @return ArrayIterator
     */
    public function getIterator(): ArrayIterator
    {
        return new ArrayIterator($this->errors);
    }
}
