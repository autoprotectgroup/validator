<?php

declare(strict_types=1);

namespace DealTrak\Validator\Validation\Error;

/**
 * Interface ErrorCollectionInterface
 *
 * @package DealTrak\Validator\Validation\Error
 */
interface ErrorCollectionInterface
{
    /**
     * @param string $message
     * @param int|null $index
     *
     * @return $this
     */
    public function addFieldErrorMessage(string $message, ?int $index = null): self;

    /**
     * @param ErrorCollectionInterface $violationCollection
     *
     * @return $this
     */
    public function addErrorCollection(ErrorCollectionInterface $violationCollection): self;

    /**
     * @param string $name
     *
     * @return $this
     */
    public function setName(string $name): self;

    /**
     * @return array
     */
    public function getErrorCollection(): array;
}
