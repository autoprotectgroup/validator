<?php

declare(strict_types=1);

namespace DealTrak\Validator\Validation;

use DealTrak\Validator\Validation\Rule\ValidationRuleInterface;
use Symfony\Component\ExpressionLanguage\ExpressionLanguage;

interface ValidationInterface
{
    /**
     * Set validation rules for the validator
     *
     * @param ValidationRuleInterface $validationRule
     *
     * @return $this
     */
    public function setValidationRules(ValidationRuleInterface $validationRule): self;

    /**
     * Validate the object
     *
     * @param object $value
     * @param boolean $validateAsEntity // needed if we want to validate the entity of the collection item
     *
     * @return iterable
     */
    public function validate(object $value, bool $validateAsEntity = true): iterable;

    /**
     * Get validation rule
     *
     * @return ValidationRuleInterface
     */
    public function getValidationRules(): ValidationRuleInterface;

    /**
     * Check if all validation passed for a given object is valid
     *
     * @return bool
     */
    public function isValid(): bool;

    /**
     * Set expression language
     *
     * @param ExpressionLanguage $expressionLanguage
     *
     * @return $this
     */
    public function setExpressionLanguage(ExpressionLanguage $expressionLanguage): self;

    /**
     * Get total errors count including multiple errors per one field
     *
     * @return int
     */
    public function getTotalFieldErrorsAmount(): int;

    /**
     * Amount of erroneous fields (every field may have several errors)
     *
     * @return int
     */
    public function getAmountOfErroneousFields(): int;
}
