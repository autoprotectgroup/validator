<?php

declare(strict_types=1);

namespace DealTrak\Validator\Validation\Builder;

use Adbar\Dot;
use DealTrak\Validator\Validation\Exception\InvalidRuleTypeException;
use DealTrak\Validator\Validation\Exception\RulesSortErrorException;
use DealTrak\Validator\Validation\Rule\Model\GeneralRuleInterface;
use DealTrak\Validator\Validation\Rule\Types\ValidationRuleType;
use DealTrak\Validator\Validation\Rule\ValidationIterableRule;
use DealTrak\Validator\Validation\Rule\ValidationRuleInterface;
use DealTrak\Validator\Validation\Rule\ValidationScalarRule;
use DealTrak\Validator\Validation\ValidationInterface;

class ValidationBuilder implements ValidationBuilderInterface
{
    protected const STRICT_TYPES = [
        ValidationRuleType::TYPE_COLLECTION,
        ValidationRuleType::TYPE_ENTITY
    ];

    private const PARENT_RULE_ENTITY_INDEX = 0;

    protected array $validationVariables = [];
    protected Dot $rawValidationRules;
    protected ValidationRuleInterface $processedNestedValidationRules;
    protected string $validationClass;

    public function __construct(string $validationClass = self::VALIDATION_CLASS_DEFAULT)
    {
        $this->setValidationClass($validationClass);
        $this->rawValidationRules = new Dot();
    }

    public function setValidationClass(string $className): self
    {
        $this->validationClass = $className;
        return $this;
    }

    /**
     * @return ValidationRuleInterface
     */
    public function getProcessedNestedValidationRules(): ValidationRuleInterface
    {
        return $this->processedNestedValidationRules;
    }

    /**
     * @return Dot
     */
    public function getRawValidationRules(): Dot
    {
        return $this->rawValidationRules;
    }

    /**
     * @param string $parameterName
     * @param $value
     *
     * @return $this
     */
    public function addValidationVariable(string $parameterName, $value): self
    {
        $this->validationVariables[$parameterName] = $value;

        return $this;
    }

    /**
     * @param GeneralRuleInterface $expression
     *
     * @return bool
     */
    private function isRuleDuplicated(GeneralRuleInterface $expression): bool
    {
        if ($this->rawValidationRules->isEmpty() || $this->rawValidationRules->isEmpty($expression->getPath())) {
            return false;
        }

        /** @var array $rules */
        $rules = $this->rawValidationRules->get($expression->getPath());

        $duplicatedRule = array_filter($rules, function ($ruleOrCategory) use ($expression): bool {

            if (!$ruleOrCategory instanceof GeneralRuleInterface && !is_array($ruleOrCategory)) {
                return false;
            }

            if (in_array($expression->getType(), self::STRICT_TYPES, true)) {
                return $this->buildRulesForExpressionAndCollections($ruleOrCategory, $expression);
            }

            return $ruleOrCategory->getPath() === $expression->getPath()
                && $ruleOrCategory->getName() === $expression->getName()
                && $ruleOrCategory->getType() === $expression->getType()
                && $ruleOrCategory->getErrorMessage() === $expression->getErrorMessage()
                && $ruleOrCategory->getExpression() === $expression->getExpression();
        });

        return !empty($duplicatedRule);
    }

    /**
     * @param $ruleOrCategory
     * @param GeneralRuleInterface $expression
     * @return bool
     */
    private function buildRulesForExpressionAndCollections($ruleOrCategory, GeneralRuleInterface $expression): bool
    {
        //In case $ruleOrCategory is not an array
        if (!is_array($ruleOrCategory)) {
            //Check if rule is duplicated for collection/entity type
            $isDuplicate = $this->isRuleDuplicatedForEntityCollectionTypes($ruleOrCategory, $expression);

            //Check if rule is duplicated but expressions are different than we add this expression to constraints
            if ($isDuplicate && $ruleOrCategory->getExpression() !== $expression->getExpression()) {
                $this->addMultipleRuleStructureToEntityCollectionRules($expression);
            }
            return $isDuplicate;
        }

        $arrRulesDuplicateStatuses = [];
        //In case $ruleOrCategory is array then firstly check whether there is duplicates with processed rule
        foreach ($ruleOrCategory as $rule) {
            if (is_array($rule)) {
                $rule = current($rule);
            }
            if (!$rule instanceof GeneralRuleInterface) {
                continue;
            }
            $isDuplicate = $this->isRuleDuplicatedForEntityCollectionTypes($rule, $expression);
            array_push($arrRulesDuplicateStatuses, $isDuplicate);
        }

        //If in array of the rule constraints there is no duplicates with processed rule
        // then add this rule to constraints
        $isDuplicate = !in_array(false, $arrRulesDuplicateStatuses);
        if ($isDuplicate) {
            $this->addMultipleRuleStructureToEntityCollectionRules($expression);
        }

        return $isDuplicate;
    }

    /**
     * @param GeneralRuleInterface $ruleOrCategory
     * @param GeneralRuleInterface $expression
     * @return bool
     */
    private function isRuleDuplicatedForEntityCollectionTypes(
        GeneralRuleInterface $ruleOrCategory,
        GeneralRuleInterface $expression
    ): bool {
        //In case collection's entity validation rule (types are not equal)
        //Then compare only path of rules to detect duplicates
        if ($ruleOrCategory->getType() !== $expression->getType()) {
            return $ruleOrCategory->getPath() === $expression->getPath();
        }

        return $ruleOrCategory->getPath() === $expression->getPath()
            && $ruleOrCategory->getName() === $expression->getName()
            && $ruleOrCategory->getType() === $expression->getType();
    }

    /**
     * This method create array structure of rule constraints for the collection/entity rule types
     *
     * @param GeneralRuleInterface $expression
     */
    private function addMultipleRuleStructureToEntityCollectionRules(GeneralRuleInterface $expression): void
    {
        //Get current rule constraint by the path from the processing expression
        $val = $this->rawValidationRules->get($expression->getPath());
        //Check if first numeric element exist in expression
        if (isset($val[self::PARENT_RULE_ENTITY_INDEX])) {
            //If first expression element is array of constraints then add processing expression to this array
            if (is_array($val[self::PARENT_RULE_ENTITY_INDEX])) {
                array_push($val[self::PARENT_RULE_ENTITY_INDEX], $expression);
            }
            //If first expression element is instance of ValidationRule than create array of rule objects
            if (is_object($val[self::PARENT_RULE_ENTITY_INDEX])) {
                $val[self::PARENT_RULE_ENTITY_INDEX] = [$val[self::PARENT_RULE_ENTITY_INDEX], $expression];
            }

            //Update validation rule data in the rules constraint array by the rule path
            $this->rawValidationRules->replace($expression->getPath(), $val);
        }
    }

    /**
     * @param GeneralRuleInterface $expression
     *
     * @return $this
     */
    public function addExpression(GeneralRuleInterface $expression): self
    {
        if (!$this->isRuleDuplicated($expression)) {
            $this->rawValidationRules->push($expression->getPath(), $expression);
        }

        return $this;
    }

    public function build(): ValidationInterface
    {
        $this->processedNestedValidationRules = $this->processStoredRules($this->rawValidationRules->all());

        return (new $this->validationClass())
            ->setValidationRules($this->processedNestedValidationRules);
    }

    /**
     * Process given raw expression rules into nested structure to be able to validate objects
     *
     * @param array $rules
     * @param ValidationRuleInterface|null $processedRule
     *
     * @return ValidationRuleInterface|null
     */
    protected function processStoredRules(
        array $rules,
        ?ValidationRuleInterface $processedRule = null
    ): ?ValidationRuleInterface {
        // this var is used to determine what is the first element's index in the array
        $elementsCount = 0;

        /**
         * @var string $expressionName
         * @var array $expression
         */
        foreach ($this->sortRulesForTheBuilder($rules) as $expressionName => $expression) {
            if ($elementsCount === 0 && !is_numeric($expressionName)) {
                $processedRule = $this->processStoredRules($expression, $processedRule);
                break;
            }

            if (!isset($processedRule) && (is_numeric($expressionName) && ((int)$expressionName) === 0)) {
                $processedRule = $this->initializeRootElement($expression);
                $elementsCount++;
                continue;
            }

            if (is_numeric($expressionName)) {
                //In case expression is array than get first element to add as constraint
                //For the scalar types of for collection/entity with one rule $expression value will be an object
                $processedRule->addExpressionConstraintRule(is_array($expression) ? current($expression) : $expression);
                $elementsCount++;
                continue;
            }

            if (!$this->isEntityOrCollection($processedRule->getType())) {
                throw new InvalidRuleTypeException(
                    sprintf(
                        'Processed rule %s with path %s should not be of type %s to accept named properties',
                        $processedRule->getName(),
                        $processedRule->getPath(),
                        $processedRule->getType()
                    )
                );
            }

            if (!is_array($expression)
                || !isset($expression[0])
                || (!$expression[0] instanceof GeneralRuleInterface && !is_array($expression[0]))
            ) {
                $this->throwBadExpression($expression, $expressionName);
            }

            /*
             * If we have more than one item with numeric key we should treat the whole collection as a list of
             * scalar rules. Any entity rules should not exist in such a list and therefore they will be ignored
             */
            if (!$this->isScalarRuleCollection($expression)) {
                $expressionRule = $this->createValidationRuleFromExpression($expression[0]);
                if ($this->isEntityOrCollection($expressionRule->getType())) {
                    $this->processStoredRules($expression, $expressionRule);
                }

                $processedRule->addExpression($expressionRule);

                $elementsCount++;
            } else {
                foreach ($expression as $key => $singleExpression) {
                    if (!$singleExpression instanceof GeneralRuleInterface) {
                        $this->throwBadExpression($expression, $key);
                    }

                    $expressionRule = $this->createValidationRuleFromExpression($singleExpression);

                    $processedRule->addExpression($expressionRule);

                    $elementsCount++;
                }
            }
        }

        return $processedRule;
    }

    /**
     * Valid patterns of $expressions collection are
     * - one entity rule with numeric key and several parent rules with string keys
     * - several scalar rules with numeric keys
     *
     * @param array $expressions
     * @return bool
     */
    private function isScalarRuleCollection(array $expressions): bool
    {
        $numericKeys = array_filter($expressions, fn($k) => is_numeric($k), ARRAY_FILTER_USE_KEY);

        return count($numericKeys) > 1;
    }

    /**
     * Check if type is valid for this kind
     *
     * @param string $type
     *
     * @return bool
     */
    private function isEntityOrCollection(string $type): bool
    {
        return in_array($type, [ValidationRuleType::TYPE_ENTITY, ValidationRuleType::TYPE_COLLECTION], true);
    }

    /**
     * Initialize root element for the nested structure
     *
     * @param GeneralRuleInterface|array $expression
     *
     * @return ValidationRuleInterface
     */
    private function initializeRootElement($expression): ValidationRuleInterface
    {
        if (!is_array($expression) && !$this->isEntityOrCollection($expression->getType())) {
            throw new InvalidRuleTypeException(
                sprintf('Expression type %s is not valid as a root element', $expression->getType())
            );
        }

        return $this->createValidationRuleFromExpression($expression);
    }

    /**
     * @param $expressionData
     * @return ValidationRuleInterface
     */
    private function createValidationRuleFromExpression($expressionData): ValidationRuleInterface
    {
        //In case expressionData is array than get first element
        $expression = is_array($expressionData) ? current($expressionData) : $expressionData;

        //Create appropriate instance of validation rule based on rule type
        $validationRule =($expression->getType() !== ValidationRuleType::TYPE_FIELD)
            ?new ValidationIterableRule():new ValidationScalarRule();

        $validationRule->setName($expression->getName())
            ->setExpression($expression->getExpression())
            ->setType($expression->getType())
            ->setPath($expression->getPath())
            ->setIsMutable($expression->isMutable())
            ->setMutableGroups($expression->getMutableGroups())
            ->setMutableFieldOverride($expression->getMutableFieldOverride())
            ->setExpressionVariables($this->validationVariables)
            ->setErrorMessage($expression->getErrorMessage() ?? '')
            ->setCollectionIndexName($expression->getCollectionIndexName());


        if (is_array($expressionData)) {
            //Add expression constraint rules in case of expressionData is array
            foreach ($expressionData as $rule) {
                $validationRule->addExpressionConstraintRule($rule);
            }
        } else {
            //Add expression constraint rule in case of expressionData is instance of ValidationRule
            $validationRule->addExpressionConstraintRule($expression);
        }

        return $validationRule;
    }

    /**
     * @param array $rules
     *
     * @return array
     */
    protected function sortRulesForTheBuilder(array $rules): array
    {
        if (!ksort($rules, SORT_NATURAL)) {
            throw new RulesSortErrorException('Rules items were not sorted');
        }
        return $rules;
    }

    /**
     * @param array $expression
     * @param string $expressionName
     */
    private function throwBadExpression(array $expression, string $expressionName): void
    {
        throw new InvalidRuleTypeException(
            sprintf(
                'Expression is not valid for this type %s and expression name %s',
                gettype($expression),
                $expressionName
            )
        );
    }
}
