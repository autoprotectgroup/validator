<?php

declare(strict_types=1);

namespace DealTrak\Validator\Validation\Builder;

use DealTrak\Validator\Validation\Rule\Model\GeneralRuleInterface;
use DealTrak\Validator\Validation\Validation;
use DealTrak\Validator\Validation\ValidationInterface;

interface ValidationBuilderInterface
{
    /**
     * Default validation class
     */
    public const VALIDATION_CLASS_DEFAULT = Validation::class;
    /**
     * Add validation expression
     *
     * @param GeneralRuleInterface $expression
     *
     * @return $this
     */
    public function addExpression(GeneralRuleInterface $expression): self;

    /**
     * Build validator
     *
     * @return ValidationInterface
     */
    public function build(): ValidationInterface;

    /**
     * Add global validation data
     *
     * @param string $parameterName
     * @param        $value
     *
     * @return $this
     */
    public function addValidationVariable(string $parameterName, $value): self;

    /**
     * Set the validation class
     *
     * @param string $className
     *
     * @return $this
     */
    public function setValidationClass(string $className): self;
}
