<?php


namespace spec\DealTrak\Validator\Validation\Builder\fixtures;

class Applicant
{
    protected string $name;

    protected string $type;

    protected Address $address;

    protected array $marketingCollection;

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @param string $name
     */
    public function setName(string $name): void
    {
        $this->name = $name;
    }

    /**
     * @return string
     */
    public function getType(): string
    {
        return $this->type;
    }

    /**
     * @param string $type
     */
    public function setType(string $type): void
    {
        $this->type = $type;
    }

    /**
     * @return Address
     */
    public function getAddress(): Address
    {
        return $this->address;
    }

    /**
     * @param Address $address
     */
    public function setAddress(Address $address): void
    {
        $this->address = $address;
    }

    /**
     * @return array
     */
    public function getMarketingCollection(): array
    {
        return $this->marketingCollection;
    }

    /**
     * @param array $marketingCollection
     */
    public function setMarketingCollection(array $marketingCollection): void
    {
        $this->marketingCollection = $marketingCollection;
    }


}

