<?php


namespace spec\DealTrak\Validator\Validation\Builder;

use DealTrak\Validator\Validation\Rule\Model\GeneralRuleInterface;
use DealTrak\Validator\Validation\Rule\Types\ValidationRuleType;
use DealTrak\Validator\Validation\Builder\ValidationBuilder;
use DealTrak\Validator\Validation\ValidationInterface;
use PhpSpec\ObjectBehavior;
use spec\DealTrak\Validator\Validation\Builder\fixtures\Applicant;

class ValidationBuilderSpec extends ObjectBehavior
{
    function let(
        GeneralRuleInterface $rule1,
        GeneralRuleInterface $rule2,
        GeneralRuleInterface $rule3,
        GeneralRuleInterface $rule4,
        GeneralRuleInterface $rule5
    ) {
        $this->setDataForStoredRules($rule1, [
            'name' => 'address',
            'type' => ValidationRuleType::TYPE_ENTITY,
            'path' => 'applicant.address',
            'expression' => '',
            'groups' => [],
            'isMutable' => false,
        ]);

        $this->setDataForStoredRules($rule2, [
            'name' => 'house',
            'type' => ValidationRuleType::TYPE_FIELD,
            'path' => 'applicant.address.house',
            'expression' => 'value > 5',
            'groups' => [],
            'isMutable' => false
        ]);

        $this->setDataForStoredRules($rule3, [
            'name' => 'house',
            'type' => ValidationRuleType::TYPE_FIELD,
            'path' => 'applicant.address.house',
            'expression' => 'value > 5',
            'groups' => [],
            'isMutable' => false
        ]);

        $this->setDataForStoredRules($rule4, [
            'name' => 'street',
            'type' => ValidationRuleType::TYPE_FIELD,
            'path' => 'applicant.address.street',
            'expression' => 'value in ["Marie", "Victoria"]',
            'groups' => [],
            'isMutable' => false
        ]);

        $this->setDataForStoredRules($rule5, [
            'name' => 'flat',
            'type' => ValidationRuleType::TYPE_FIELD,
            'path' => 'applicant.address.flat',
            'expression' => 'value < 30',
            'groups' => [],
            'isMutable' => false
        ]);
    }

    function it_is_initializable()
    {
        $this->shouldHaveType(ValidationBuilder::class);
    }

    function it_should_remove_duplicated_rules(
        GeneralRuleInterface $rule1,
        GeneralRuleInterface $rule5,
        GeneralRuleInterface $rule6,
        GeneralRuleInterface $rule7,
        GeneralRuleInterface $rule8,
        GeneralRuleInterface $rule10
    ) {
        $this->setDataForStoredRules($rule6, [
            'name' => 'flat',
            'type' => ValidationRuleType::TYPE_FIELD,
            'path' => 'applicant.address.flat',
            'expression' => 'value > 50',
            'groups' => [],
            'isMutable' => false
        ]);

        $this->setDataForStoredRules($rule7, [
            'name' => 'assets',
            'type' => ValidationRuleType::TYPE_COLLECTION,
            'path' => 'applicant.assets',
            'expression' => 'count(value) > 50',
            'groups' => [],
            'isMutable' => false
        ]);

        $this->setDataForStoredRules($rule8, [
            'name' => 'assets',
            'type' => ValidationRuleType::TYPE_COLLECTION,
            'path' => 'applicant.assets',
            'expression' => 'count(value) < 50',
            'groups' => [],
            'isMutable' => false
        ]);

        $this->setDataForStoredRules($rule10, [
            'name' => 'assets',
            'type' => ValidationRuleType::TYPE_COLLECTION,
            'path' => 'applicant.assets',
            'expression' => 'count(value) > 600',
            'groups' => [],
            'isMutable' => false
        ]);

        $this
            ->addExpression($rule1)
            ->addExpression($rule5)
            ->addExpression($rule1)
            ->addExpression($rule6)
            ->addExpression($rule5)
            ->addExpression($rule7)
            ->addExpression($rule1)
            ->addExpression($rule7)
            ->addExpression($rule8)
            ->addExpression($rule10)
        ;

        $this->getRawValidationRules()
            ->all()
            ->shouldBe(
                [
                    'applicant' => [
                        'address' => [
                            $rule1,
                            'flat' => [
                                $rule5,
                                $rule6,
                            ]
                        ],
                        'assets' => [
                            [$rule7,$rule8,$rule10],
                        ]
                    ]
                ]
            );
    }

    function it_should_return_build_validation(
        GeneralRuleInterface $rule1,
        GeneralRuleInterface $rule2,
        GeneralRuleInterface $rule3,
        GeneralRuleInterface $rule4,
        GeneralRuleInterface $rule5
    ) {
        $this
            ->addExpression($rule1)
            ->addExpression($rule2)
            ->addExpression($rule3)
            ->addExpression($rule4)
            ->addExpression($rule5)
        ;

        $this->build()->shouldReturnAnInstanceOf(ValidationInterface::class);
    }

    function it_should_set_the_rules_in_a_random_order(
        GeneralRuleInterface $rule1,
        GeneralRuleInterface $rule2,
        GeneralRuleInterface $rule3,
        GeneralRuleInterface $rule4,
        GeneralRuleInterface $rule5
    ) {
        $this
            ->addExpression($rule2)
            ->addExpression($rule3)
            ->addExpression($rule4)
            ->addExpression($rule5)
            ->addExpression($rule1)
        ;

        $this->build()->shouldReturnAnInstanceOf(ValidationInterface::class);
    }

    function it_can_add_stored_rules_having_a_nested_object (
        GeneralRuleInterface $rule1,
        GeneralRuleInterface $rule2,
        GeneralRuleInterface $rule3,
        GeneralRuleInterface $rule4,
        GeneralRuleInterface $rule5,
        GeneralRuleInterface $rule6,
        GeneralRuleInterface $rule7
    ) {
        $this->setDataForStoredRules($rule6, [
            'name' => 'marketing',
            'type' => ValidationRuleType::TYPE_ENTITY,
            'path' => 'applicant.address.marketing',
            'expression' => '',
            'groups' => [],
            'isMutable' => false,
        ]);

        $this->setDataForStoredRules($rule7, [
            'name' => 'mobileNumber',
            'type' => ValidationRuleType::TYPE_FIELD,
            'path' => 'applicant.address.marketing.mobileNumber',
            'expression' => 'value > 1000',
            'groups' => [],
            'isMutable' => false,
        ]);

        $this
            ->addExpression($rule1)
            ->addExpression($rule2)
            ->addExpression($rule3)
            ->addExpression($rule4)
            ->addExpression($rule5)
            ->addExpression($rule6)
            ->addExpression($rule7)
        ;

        $this->build()->shouldReturnAnInstanceOf(ValidationInterface::class);
    }

    function it_can_add_stored_rules_having_a_collection (
        GeneralRuleInterface $rule1,
        GeneralRuleInterface $rule2,
        GeneralRuleInterface $rule3,
        GeneralRuleInterface $rule4,
        GeneralRuleInterface $rule5,
        GeneralRuleInterface $rule6,
        GeneralRuleInterface $rule7
    ) {
        $this->setDataForStoredRules($rule6, [
            'name' => 'marketingCollection',
            'type' => ValidationRuleType::TYPE_COLLECTION,
            'path' => 'applicant.address.marketingCollection',
            'expression' => '',
            'groups' => [],
            'isMutable' => false,
        ]);

        $this->setDataForStoredRules($rule7, [
            'name' => 'mobileNumber',
            'type' => ValidationRuleType::TYPE_FIELD,
            'path' => 'applicant.address.marketingCollection.mobileNumber',
            'expression' => 'value > 1000',
            'groups' => [],
            'isMutable' => false
        ]);

        $this
            ->addExpression($rule1)
            ->addExpression($rule2)
            ->addExpression($rule3)
            ->addExpression($rule4)
            ->addExpression($rule5)
            ->addExpression($rule6)
            ->addExpression($rule7)
        ;

        $this->build()->shouldReturnAnInstanceOf(ValidationInterface::class);
    }

    function it_can_add_global_data_parameters(
        GeneralRuleInterface $rule1,
        GeneralRuleInterface $rule2,
        GeneralRuleInterface $rule3,
        GeneralRuleInterface $rule4,
        GeneralRuleInterface $rule5
    )    {
        $this
            ->addExpression($rule1)
            ->addExpression($rule2)
            ->addExpression($rule3)
            ->addExpression($rule4)
            ->addExpression($rule5)
        ;

        $this->addValidationVariable('applicant', new Applicant())
             ->build()->shouldReturnAnInstanceOf(ValidationInterface::class)
        ;
    }

    /**
     * @param GeneralRuleInterface $storedRule
     * @param array                $data
     */
    protected function setDataForStoredRules(GeneralRuleInterface $storedRule, array $data)
    {
        $storedRule->getName()->willReturn($data['name']);
        $storedRule->getType()->willReturn($data['type']);
        $storedRule->getPath()->willReturn($data['path']);
        $storedRule->getExpression()->willReturn($data['expression']);
        $storedRule->getMutableGroups()->willReturn($data['groups']);
        $storedRule->getMutableFieldOverride()->willReturn($data['mutableFieldOverride'] ?? null);
        $storedRule->isMutable()->willReturn($data['isMutable']);
        $storedRule->getErrorMessage()->willReturn($data['errorMessage'] ?? '');
        $storedRule->getCollectionIndexName()->willReturn($data['collectionIndexName'] ?? '');
    }

}
