<?php


namespace spec\DealTrak\Validator\Validation;

use ArrayIterator;
use DealTrak\Validator\Validation\Builder\ValidationBuilder;
use DealTrak\Validator\Validation\Exception\NoMutatorFoundException;
use DealTrak\Validator\Validation\Rule\Model\GeneralRuleInterface;
use DealTrak\Validator\Validation\Rule\Types\ValidationRuleType;
use DealTrak\Validator\Validation\Rule\ValidationRuleInterface;
use DealTrak\Validator\Validation\Validation;
use DealTrak\Validator\Validation\Error\ErrorCollection;
use PhpSpec\ObjectBehavior;
use spec\DealTrak\Validator\Validation\fixtures\Address;
use spec\DealTrak\Validator\Validation\fixtures\Applicant;
use spec\DealTrak\Validator\Validation\fixtures\ApplicantType;
use spec\DealTrak\Validator\Validation\fixtures\Marketing;
use spec\DealTrak\Validator\Validation\fixtures\validWithCollection\ApplicantWithAddressCollection;
use spec\DealTrak\Validator\Validation\fixtures\validWithGlobalVariable\Deal;

class ValidationSpec extends ObjectBehavior
{
    function it_is_initializable()
    {
        $this->shouldHaveType(Validation::class);
    }

    public function it_can_validate_a_valid_object(
        GeneralRuleInterface $storedRuleApplicant,
        GeneralRuleInterface $storedRuleApplicantName,
        GeneralRuleInterface $storedRuleApplicantType,
        GeneralRuleInterface $storedRuleApplicantAddress,
        GeneralRuleInterface $storedRuleApplicantAddressHouse1,
        GeneralRuleInterface $storedRuleApplicantAddressHouse2,
        GeneralRuleInterface $storedRuleApplicantAddressStreet,
        GeneralRuleInterface $storedRuleApplicantAddressFlat,
        GeneralRuleInterface $storedRuleApplicantAddressMarketing,
        GeneralRuleInterface $storedRuleApplicantAddressMarketingMobileNumber
    ) {
        $applicant = $this->getMockDataForValidation();

        $validationBuilder = (new ValidationBuilder())
            ->addExpression($storedRuleApplicant->getWrappedObject())
            ->addExpression($storedRuleApplicantName->getWrappedObject())
            ->addExpression($storedRuleApplicantType->getWrappedObject())
            ->addExpression($storedRuleApplicantAddress->getWrappedObject())
            ->addExpression($storedRuleApplicantAddressHouse1->getWrappedObject())
            ->addExpression($storedRuleApplicantAddressHouse2->getWrappedObject())
            ->addExpression($storedRuleApplicantAddressStreet->getWrappedObject())
            ->addExpression($storedRuleApplicantAddressFlat->getWrappedObject())
            ->addExpression($storedRuleApplicantAddressMarketing->getWrappedObject())
            ->addExpression($storedRuleApplicantAddressMarketingMobileNumber->getWrappedObject());

        $validationBuilder->build();

        $this->setValidationRules($validationBuilder->getProcessedNestedValidationRules());

        $violations = $this->validate($applicant);
        $violations->shouldReturnAnInstanceOf(ErrorCollection::class);

        $this->isValid()->shouldReturn(true);
    }

    public function it_can_use_several_rules_for_one_field(
        GeneralRuleInterface $storedRuleApplicant,
        GeneralRuleInterface $storedRuleApplicantName,
        GeneralRuleInterface $storedRuleApplicantType,
        GeneralRuleInterface $storedRuleApplicantAddress,
        GeneralRuleInterface $storedRuleApplicantAddressHouse1,
        GeneralRuleInterface $storedRuleApplicantAddressHouse2,
        GeneralRuleInterface $storedRuleApplicantAddressStreet,
        GeneralRuleInterface $storedRuleApplicantAddressFlat,
        GeneralRuleInterface $storedRuleApplicantAddressMarketing,
        GeneralRuleInterface $storedRuleApplicantAddressMarketingMobileNumber
    ) {
        $this->setDataForStoredRules($storedRuleApplicantAddressHouse2, [
            'name' => 'house',
            'type' => ValidationRuleType::TYPE_FIELD,
            'path' => 'applicant.address.house',
            'expression' => 'value < 40',
            'mutableGroups' => [],
            'isMutable' => false
        ]);

        $applicant = $this->getMockDataForValidation();

        $validationBuilder = (new ValidationBuilder())
            ->addExpression($storedRuleApplicant->getWrappedObject())
            ->addExpression($storedRuleApplicantName->getWrappedObject())
            ->addExpression($storedRuleApplicantType->getWrappedObject())
            ->addExpression($storedRuleApplicantAddress->getWrappedObject())
            ->addExpression($storedRuleApplicantAddressHouse1->getWrappedObject())
            ->addExpression($storedRuleApplicantAddressHouse2->getWrappedObject())
            ->addExpression($storedRuleApplicantAddressStreet->getWrappedObject())
            ->addExpression($storedRuleApplicantAddressFlat->getWrappedObject())
            ->addExpression($storedRuleApplicantAddressMarketing->getWrappedObject())
            ->addExpression($storedRuleApplicantAddressMarketingMobileNumber->getWrappedObject());

        $validationBuilder->build();

        $this->setValidationRules($validationBuilder->getProcessedNestedValidationRules());

        $violations = $this->validate($applicant);
        $violations->shouldReturnAnInstanceOf(ErrorCollection::class);

        $this->isValid()->shouldReturn(false);
    }

    function it_can_validate_a_valid_object_with_global_variable(
        GeneralRuleInterface $storedRuleApplicant,
        GeneralRuleInterface $storedRuleApplicantName,
        GeneralRuleInterface $storedRuleApplicantType,
        GeneralRuleInterface $storedRuleApplicantAddress,
        GeneralRuleInterface $storedRuleApplicantAddressHouse1,
        GeneralRuleInterface $storedRuleApplicantAddressHouse2,
        GeneralRuleInterface $storedRuleApplicantAddressStreet,
        GeneralRuleInterface $storedRuleApplicantAddressFlat,
        GeneralRuleInterface $storedRuleApplicantAddressMarketing,
        GeneralRuleInterface $storedRuleApplicantAddressMarketingMobileNumber
    ) {
        $this->setDataForStoredRules($storedRuleApplicantAddressStreet, [
            'name' => 'street',
            'type' => ValidationRuleType::TYPE_FIELD,
            'path' => 'applicant.address.street',
            'expression' =>
                'value in [deal.getApplicants()[applicantId].getName(), deal.getApplicants()[applicantId].getType().name]',
            'mutableGroups' => [],
            'isMutable' => false
        ]);

        $applicantForValidation = $this->getMockDataForValidation();

        $deal = new Deal();

        $applicantTwo = $this->getMockDataForValidation();
        $applicantTwo->setType(ApplicantType::Marie);

        $deal->setApplicantType('private');
        $deal->setApplicants([
            'applicantOne' => $this->getMockDataForValidation(),
            'applicantTwo' => $applicantTwo,
        ]);

        $validationBuilder = (new ValidationBuilder())
            ->addValidationVariable('deal', $deal)
            ->addValidationVariable('applicantId', 'applicantTwo')
            ->addExpression($storedRuleApplicant->getWrappedObject())
            ->addExpression($storedRuleApplicantName->getWrappedObject())
            ->addExpression($storedRuleApplicantType->getWrappedObject())
            ->addExpression($storedRuleApplicantAddress->getWrappedObject())
            ->addExpression($storedRuleApplicantAddressHouse1->getWrappedObject())
            ->addExpression($storedRuleApplicantAddressHouse2->getWrappedObject())
            ->addExpression($storedRuleApplicantAddressStreet->getWrappedObject())
            ->addExpression($storedRuleApplicantAddressFlat->getWrappedObject())
            ->addExpression($storedRuleApplicantAddressMarketing->getWrappedObject())
            ->addExpression($storedRuleApplicantAddressMarketingMobileNumber->getWrappedObject());

        $validationBuilder->build();

        $this->setValidationRules($validationBuilder->getProcessedNestedValidationRules());

        $violations = $this->validate($applicantForValidation);
        $violations->shouldReturnAnInstanceOf(ErrorCollection::class);

        $this->isValid()->shouldReturn(true);
    }

    function it_can_validate_a_valid_object_with_field_accessors(
        GeneralRuleInterface $storedRuleApplicant,
        GeneralRuleInterface $storedRuleApplicantName,
        GeneralRuleInterface $storedRuleApplicantType,
        GeneralRuleInterface $storedRuleApplicantAddress,
        GeneralRuleInterface $storedRuleApplicantAddressHouse1,
        GeneralRuleInterface $storedRuleApplicantAddressHouse2,
        GeneralRuleInterface $storedRuleApplicantAddressStreet,
        GeneralRuleInterface $storedRuleApplicantAddressFlat,
        GeneralRuleInterface $storedRuleApplicantAddressMarketing,
        GeneralRuleInterface $storedRuleApplicantAddressMarketingMobileNumber
    ) {
        $this->setDataForStoredRules($storedRuleApplicantAddressStreet, [
            'name' => 'street',
            'type' => ValidationRuleType::TYPE_FIELD,
            'path' => 'applicant.address.street',
            'expression' =>
                'value in [deal.applicants[applicantId].name, deal.applicants[applicantId].type.name]',
            'mutableGroups' => [],
            'isMutable' => false
        ]);

        $applicantForValidation = $this->getMockDataForValidation();

        $deal = new Deal();

        $applicantTwo = $this->getMockDataForValidation();
        $applicantTwo->setType(ApplicantType::Marie);

        $deal->setApplicantType('private');
        $deal->setApplicants([
            'applicantOne' => $this->getMockDataForValidation(),
            'applicantTwo' => $applicantTwo,
        ]);

        $validationBuilder = (new ValidationBuilder())
            ->addValidationVariable('deal', $deal)
            ->addValidationVariable('applicantId', 'applicantTwo')
            ->addExpression($storedRuleApplicant->getWrappedObject())
            ->addExpression($storedRuleApplicantName->getWrappedObject())
            ->addExpression($storedRuleApplicantType->getWrappedObject())
            ->addExpression($storedRuleApplicantAddress->getWrappedObject())
            ->addExpression($storedRuleApplicantAddressHouse1->getWrappedObject())
            ->addExpression($storedRuleApplicantAddressHouse2->getWrappedObject())
            ->addExpression($storedRuleApplicantAddressStreet->getWrappedObject())
            ->addExpression($storedRuleApplicantAddressFlat->getWrappedObject())
            ->addExpression($storedRuleApplicantAddressMarketing->getWrappedObject())
            ->addExpression($storedRuleApplicantAddressMarketingMobileNumber->getWrappedObject());

        $validationBuilder->build();

        $this->setValidationRules($validationBuilder->getProcessedNestedValidationRules());

        $violations = $this->validate($applicantForValidation);
        $violations->shouldReturnAnInstanceOf(ErrorCollection::class);

        $this->isValid()->shouldReturn(true);
    }

    function it_can_validate_a_invalid_object_with_field_accessors(
        GeneralRuleInterface $storedRuleApplicant,
        GeneralRuleInterface $storedRuleApplicantName,
        GeneralRuleInterface $storedRuleApplicantType,
        GeneralRuleInterface $storedRuleApplicantAddress,
        GeneralRuleInterface $storedRuleApplicantAddressHouse1,
        GeneralRuleInterface $storedRuleApplicantAddressHouse2,
        GeneralRuleInterface $storedRuleApplicantAddressStreet,
        GeneralRuleInterface $storedRuleApplicantAddressFlat,
        GeneralRuleInterface $storedRuleApplicantAddressMarketing,
        GeneralRuleInterface $storedRuleApplicantAddressMarketingMobileNumber
    ) {
        $this->setDataForStoredRules($storedRuleApplicantAddressStreet, [
            'name' => 'street',
            'type' => ValidationRuleType::TYPE_FIELD,
            'path' => 'applicant.address.street',
            'expression' =>
                'value in [deal.applicant[applicantId].name, deal.applicants[applicantId].type]',
            'mutableGroups' => [],
            'isMutable' => false
        ]);

        $applicantForValidation = $this->getMockDataForValidation();

        $deal = new Deal();

        $applicantTwo = $this->getMockDataForValidation();
        $applicantTwo->setType(ApplicantType::Marie);

        $deal->setApplicantType('private');
        $deal->setApplicants([
            'applicantOne' => $this->getMockDataForValidation(),
            'applicantTwo' => $applicantTwo,
        ]);

        $validationBuilder = (new ValidationBuilder())
            ->addValidationVariable('deal', $deal)
            ->addValidationVariable('applicantId', 'applicantTwo')
            ->addExpression($storedRuleApplicant->getWrappedObject())
            ->addExpression($storedRuleApplicantName->getWrappedObject())
            ->addExpression($storedRuleApplicantType->getWrappedObject())
            ->addExpression($storedRuleApplicantAddress->getWrappedObject())
            ->addExpression($storedRuleApplicantAddressHouse1->getWrappedObject())
            ->addExpression($storedRuleApplicantAddressHouse2->getWrappedObject())
            ->addExpression($storedRuleApplicantAddressStreet->getWrappedObject())
            ->addExpression($storedRuleApplicantAddressFlat->getWrappedObject())
            ->addExpression($storedRuleApplicantAddressMarketing->getWrappedObject())
            ->addExpression($storedRuleApplicantAddressMarketingMobileNumber->getWrappedObject());

        $validationBuilder->build();

        $this->setValidationRules($validationBuilder->getProcessedNestedValidationRules());

        $violations = $this->validate($applicantForValidation);
        $violations->shouldReturnAnInstanceOf(ErrorCollection::class);

        $this->isValid()->shouldReturn(false);
        $this->getAmountOfErroneousFields()->shouldReturn(1);
    }

    function it_can_validate_objects_containing_collections(
        GeneralRuleInterface $storedRuleApplicant,
        GeneralRuleInterface $storedRuleApplicantName,
        GeneralRuleInterface $storedRuleApplicantType,
        GeneralRuleInterface $storedRuleApplicantAddress,
        GeneralRuleInterface $storedRuleApplicantAddressHouse1,
        GeneralRuleInterface $storedRuleApplicantAddressHouse2,
        GeneralRuleInterface $storedRuleApplicantAddressStreet,
        GeneralRuleInterface $storedRuleApplicantAddressFlat,
        GeneralRuleInterface $storedRuleApplicantAddressMarketing,
        GeneralRuleInterface $storedRuleApplicantAddressMarketingMobileNumber
    ) {
        $applicantForValidation = $this->getMockApplicantWithAddressCollectionDataForValidation();

        $this->setDataForStoredRules($storedRuleApplicant, [
            'name' => 'applicant',
            'type' => ValidationRuleType::TYPE_ENTITY,
            'path' => 'applicant',
            'expression' => 'value.getName() === "Andy"',
            'mutableGroups' => [],
            'isMutable' => false,
        ]);

        $this->setDataForStoredRules($storedRuleApplicantName, [
            'name' => 'name',
            'type' => ValidationRuleType::TYPE_FIELD,
            'path' => 'applicant.name',
            'expression' => 'value !== null',
            'mutableGroups' => [],
            'isMutable' => false,
        ]);

        $this->setDataForStoredRules($storedRuleApplicantAddressStreet, [
            'name' => 'street',
            'type' => ValidationRuleType::TYPE_FIELD,
            'path' => 'applicant.address.street',
            'expression' =>
                'value in [deal.getApplicants()[applicantId].getAddress()[addressId].getStreet()]',
            'mutableGroups' => [],
            'isMutable' => false
        ]);

        $this->setDataForStoredRules($storedRuleApplicantAddress, [
            'name' => 'address',
            'type' => ValidationRuleType::TYPE_COLLECTION,
            'path' => 'applicant.address',
            'expression' => 'value.count() > 0',
            'mutableGroups' => [],
            'isMutable' => false,
            'collectionIndexName' => 'addressId',
        ]);

        $deal = new Deal();

        $applicantTwo = $this->getMockApplicantWithAddressCollectionDataForValidation();
        $applicantTwo->setType(ApplicantType::Marie);

        $deal->setApplicantType('private');
        $deal->setApplicants([
            'applicantOne' => $this->getMockApplicantWithAddressCollectionDataForValidation(),
            'applicantTwo' => $applicantTwo,
        ]);

        $validationBuilder = (new ValidationBuilder())
            ->addValidationVariable('deal', $deal)
            ->addValidationVariable('applicantId', 'applicantTwo')
            ->addExpression($storedRuleApplicant->getWrappedObject())
            ->addExpression($storedRuleApplicantName->getWrappedObject())
            ->addExpression($storedRuleApplicantType->getWrappedObject())
            ->addExpression($storedRuleApplicantAddress->getWrappedObject())
            ->addExpression($storedRuleApplicantAddressHouse1->getWrappedObject())
            ->addExpression($storedRuleApplicantAddressHouse2->getWrappedObject())
            ->addExpression($storedRuleApplicantAddressStreet->getWrappedObject())
            ->addExpression($storedRuleApplicantAddressFlat->getWrappedObject())
            ->addExpression($storedRuleApplicantAddressMarketing->getWrappedObject())
            ->addExpression($storedRuleApplicantAddressMarketingMobileNumber->getWrappedObject());

        $validationBuilder->build();

        $this->setValidationRules($validationBuilder->getProcessedNestedValidationRules());

        $violations = $this->validate($applicantForValidation);
        $violations->shouldReturnAnInstanceOf(ErrorCollection::class);

        $this->isValid()->shouldReturn(true);
    }

    function it_can_validate_collections(
        GeneralRuleInterface $storedRuleApplicantAddress,
        GeneralRuleInterface $storedRuleApplicantAddressHouse1,
        GeneralRuleInterface $storedRuleApplicantAddressHouse2,
        GeneralRuleInterface $storedRuleApplicantAddressStreet,
        GeneralRuleInterface $storedRuleApplicantAddressFlat,
        GeneralRuleInterface $storedRuleApplicantAddressMarketing,
        GeneralRuleInterface $storedRuleApplicantAddressMarketingMobileNumber
    ) {
        $applicantForValidation = $this->getMockApplicantWithAddressCollectionDataForValidation();

        $this->setDataForStoredRules($storedRuleApplicantAddress, [
            'name' => 'address',
            'type' => ValidationRuleType::TYPE_COLLECTION,
            'path' => 'applicant.address',
            'expression' => 'value.count() <= 2',
            'mutableGroups' => [],
            'isMutable' => false,
            'collectionIndexName' => 'addressId',
        ]);

        $this->setDataForStoredRules($storedRuleApplicantAddressStreet, [
            'name' => 'street',
            'type' => ValidationRuleType::TYPE_FIELD,
            'path' => 'applicant.address.street',
            'expression' =>
                'value in ["Marie"]',
            'mutableGroups' => [],
            'isMutable' => false
        ]);

        $this->setDataForStoredRules($storedRuleApplicantAddressFlat, [
            'name' => 'flat',
            'type' => ValidationRuleType::TYPE_FIELD,
            'path' => 'applicant.address.flat',
            'expression' =>
                'value < 30',
            'mutableGroups' => [],
            'isMutable' => false
        ]);

        $this->setDataForStoredRules($storedRuleApplicantAddressMarketing, [
            'name' => 'marketing',
            'type' => ValidationRuleType::TYPE_ENTITY,
            'path' => 'applicant.address.marketing',
            'expression' => 'value.getMobileNumber() > 100',
            'mutableGroups' => [],
            'isMutable' => false
        ]);

        $this->setDataForStoredRules($storedRuleApplicantAddressMarketingMobileNumber, [
            'name' => 'mobileNumber',
            'type' => ValidationRuleType::TYPE_FIELD,
            'path' => 'applicant.address.marketing.mobileNumber',
            'expression' => 'value > 1000',
            'mutableGroups' => [],
            'isMutable' => false,
        ]);

        $deal = new Deal();

        $applicantTwo = $this->getMockApplicantWithAddressCollectionDataForValidation();
        $applicantTwo->setType(ApplicantType::Marie);

        $deal->setApplicantType('private');
        $deal->setApplicants([
            'applicantOne' => $this->getMockApplicantWithAddressCollectionDataForValidation(),
            'applicantTwo' => $applicantTwo,
        ]);

        $validationBuilder = (new ValidationBuilder())
            ->addValidationVariable('deal', $deal)
            ->addValidationVariable('applicantId', 'applicantTwo')
            ->addExpression($storedRuleApplicantAddress->getWrappedObject())
            ->addExpression($storedRuleApplicantAddressHouse1->getWrappedObject())
            ->addExpression($storedRuleApplicantAddressHouse2->getWrappedObject())
            ->addExpression($storedRuleApplicantAddressStreet->getWrappedObject())
            ->addExpression($storedRuleApplicantAddressFlat->getWrappedObject())
            ->addExpression($storedRuleApplicantAddressMarketing->getWrappedObject())
            ->addExpression($storedRuleApplicantAddressMarketingMobileNumber->getWrappedObject());

        $validationBuilder->build();

        $this->setValidationRules($validationBuilder->getProcessedNestedValidationRules());
        $violations = $this->validate(new ArrayIterator($applicantForValidation->getAddress()), false);

        $violations->shouldReturnAnInstanceOf(ErrorCollection::class);

        $this->isValid()->shouldReturn(true);
    }

    function it_can_return_error_collections_with_nested_structure(
        GeneralRuleInterface $storedRuleApplicant,
        GeneralRuleInterface $storedRuleApplicantName,
        GeneralRuleInterface $storedRuleApplicantType,
        GeneralRuleInterface $storedRuleApplicantAddress,
        GeneralRuleInterface $storedRuleApplicantAddressHouse1,
        GeneralRuleInterface $storedRuleApplicantAddressHouse2,
        GeneralRuleInterface $storedRuleApplicantAddressStreet,
        GeneralRuleInterface $storedRuleApplicantAddressFlat,
        GeneralRuleInterface $storedRuleApplicantAddressMarketing,
        GeneralRuleInterface $storedRuleApplicantAddressMarketingMobileNumber
    ) {
        $this->setDataForStoredRules($storedRuleApplicant, [
            'name' => 'applicant',
            'type' => ValidationRuleType::TYPE_ENTITY,
            'path' => 'applicant',
            'expression' => 'value.name === "Andy"',
            'mutableGroups' => [],
            'isMutable' => true,
        ]);

        $this->setDataForStoredRules($storedRuleApplicantAddressHouse2, [
            'name' => 'house',
            'type' => ValidationRuleType::TYPE_FIELD,
            'path' => 'applicant.address.house',
            'expression' => 'value >= 40',
            'mutableGroups' => [],
            'isMutable' => false
        ]);

        $applicant = $this->getMockDataForValidation();

        $deal = new Deal();

        $validationBuilder = (new ValidationBuilder())
            ->addValidationVariable('deal', $deal)
            ->addExpression($storedRuleApplicant->getWrappedObject())
            ->addExpression($storedRuleApplicantName->getWrappedObject())
            ->addExpression($storedRuleApplicantType->getWrappedObject())
            ->addExpression($storedRuleApplicantAddress->getWrappedObject())
            ->addExpression($storedRuleApplicantAddressHouse1->getWrappedObject())
            ->addExpression($storedRuleApplicantAddressStreet->getWrappedObject())
            ->addExpression($storedRuleApplicantAddressFlat->getWrappedObject())
            ->addExpression($storedRuleApplicantAddressMarketing->getWrappedObject())
            ->addExpression($storedRuleApplicantAddressMarketingMobileNumber->getWrappedObject());

        $validationBuilder->build();
        $this->setValidationRules($validationBuilder->getProcessedNestedValidationRules());

        $violations = $this->validate($applicant);
        $this->isValid()->shouldReturn(true);

        $violations->shouldReturnAnInstanceOf(ErrorCollection::class);
        $errorCollection = $violations->getErrorCollection();
        $errorCollection->shouldBeArray();



        //Validate Applicant error collection structure
        $violations->getName()->shouldBe('applicant');


        //Validate Address error collection structure
        /** @var ErrorCollection $addressErrorCollection */
        $addressErrorCollection =  $errorCollection[0];
        $addressErrorCollection->getName()->shouldBe('address');
        $addressErrorCollection->getErrorCollection()->shouldBeArray();
        $addressErrorCollection->getErrorCollection()[0]->getName()->shouldBe('flat');
        $addressErrorCollection->getErrorCollection()[1]->getName()->shouldBe('house');
        $addressErrorCollection->getErrorCollection()[2]->getName()->shouldBe('marketing');
        $addressErrorCollection->getErrorCollection()[3]->getName()->shouldBe('street');

        //Validate Marketing error collection structure
        /** @var ErrorCollection $marketingErrorCollection */
        $marketingErrorCollection = $addressErrorCollection->getErrorCollection()[2];
        $marketingErrorCollection->getName()->shouldBe('marketing');
        $marketingErrorCollection->getErrorCollection()->shouldBeArray();
        $marketingErrorCollection->getErrorCollection()[0]->getName()->shouldBe('mobileNumber');
    }

    function it_can_return_error_collections_multiple_errors_on_collection_type(
        GeneralRuleInterface $storedRuleApplicantAddressFirstRule,
        GeneralRuleInterface $storedRuleApplicantAddressSecondRule
    ) {
        $this->setDataForStoredRules($storedRuleApplicantAddressFirstRule, [
            'name' => 'address',
            'type' => ValidationRuleType::TYPE_COLLECTION,
            'path' => 'applicant.address',
            'expression' => 'value.house > 45',
            'errorMessage' => 'House number should be more than 45',
            'mutableGroups' => [],
            'isMutable' => false,
            'collectionIndexName' => 'addressId',
        ]);

        $this->setDataForStoredRules($storedRuleApplicantAddressSecondRule, [
            'name' => 'address',
            'type' => ValidationRuleType::TYPE_COLLECTION,
            'path' => 'applicant.address',
            'expression' => 'value.flat === 15',
            'errorMessage' => 'Flat number should be equal to 15',
            'mutableGroups' => [],
            'isMutable' => false,
            'collectionIndexName' => 'addressId',
        ]);

        $applicant = $this->getMockDataForValidation();

        $deal = new Deal();

        $validationBuilder = (new ValidationBuilder())
            ->addValidationVariable('deal', $deal)
            ->addExpression($storedRuleApplicantAddressFirstRule->getWrappedObject())
            ->addExpression($storedRuleApplicantAddressSecondRule->getWrappedObject())
        ;

        $validationBuilder->build();
        $this->setValidationRules($validationBuilder->getProcessedNestedValidationRules());
        $violations = $this->validate($applicant);
        $this->isValid()->shouldReturn(false);
        $violations->shouldReturnAnInstanceOf(ErrorCollection::class);
        $errorCollection = $violations->getErrorCollection();
        $errorCollection->shouldBeArray();

        //Validate Address error collection structure
        $violations->getName()->shouldBe('address');
        $violations->getFieldErrorMessages()->shouldBe(
            [
                'House number should be more than 45',
                'Flat number should be equal to 15'
            ]
        );
    }

    function it_can_return_error_collections_multiple_errors_on_entity_type(
        GeneralRuleInterface $storedRuleApplicantFirstRule,
        GeneralRuleInterface $storedRuleApplicantSecondRule
    ) {
        $this->setDataForStoredRules($storedRuleApplicantFirstRule, [
            'name' => 'applicant',
            'type' => ValidationRuleType::TYPE_ENTITY,
            'path' => 'applicant',
            'expression' => '',
            'mutableGroups' => [],
            'errorMessage' => '',
            'isMutable' => false,
        ]);

        $this->setDataForStoredRules($storedRuleApplicantFirstRule, [
            'name' => 'applicant',
            'type' => ValidationRuleType::TYPE_ENTITY,
            'path' => 'applicant',
            'expression' => 'value.name !== "Andy"',
            'mutableGroups' => [],
            'errorMessage' => 'Applicant name should not be equal Andy',
            'isMutable' => false,
        ]);

        $this->setDataForStoredRules($storedRuleApplicantSecondRule, [
            'name' => 'applicant',
            'type' => ValidationRuleType::TYPE_ENTITY,
            'path' => 'applicant',
            'expression' => 'value.type.name !== "Good"',
            'mutableGroups' => [],
            'errorMessage' => 'Applicant type should not be equal Good',
            'isMutable' => false,
        ]);

        $applicant = $this->getMockDataForValidation();

        $deal = new Deal();

        $validationBuilder = (new ValidationBuilder())
            ->addValidationVariable('deal', $deal)
            ->addExpression($storedRuleApplicantFirstRule->getWrappedObject())
            ->addExpression($storedRuleApplicantSecondRule->getWrappedObject())
        ;

        $validationBuilder->build();
        $this->setValidationRules($validationBuilder->getProcessedNestedValidationRules());

        $violations = $this->validate($applicant);
        $this->isValid()->shouldReturn(false);

        $violations->shouldReturnAnInstanceOf(ErrorCollection::class);
        $errorCollection = $violations->getErrorCollection();
        $errorCollection->shouldBeArray();

        $violations->getName()->shouldBe('applicant');
        $violations->getFieldErrorMessages()->shouldBe(
            [
                'Applicant name should not be equal Andy',
                'Applicant type should not be equal Good'
            ]
        );
    }


    function it_can_return_error_collections_multiple_errors_on_collection_entity_type(
        GeneralRuleInterface $storedRuleApplicantAddressCollection,
        GeneralRuleInterface $storedRuleApplicantAddressCollectionEntity,
        GeneralRuleInterface $storedRuleApplicantAddressCollectionEntityTwo,
        GeneralRuleInterface $storedRuleApplicantAddressCollectionEntityThree
    ) {
        $applicantForValidation = $this->getMockApplicantWithAddressCollectionDataForValidation();
        $this->setDataForStoredRules($storedRuleApplicantAddressCollection, [
            'name' => 'address',
            'type' => ValidationRuleType::TYPE_COLLECTION,
            'path' => 'applicant.address',
            'expression' => 'value.count() <= 2',
            'mutableGroups' => [],
            'isMutable' => false,
            'collectionIndexName' => 'addressId',
        ]);

        $this->setDataForStoredRules($storedRuleApplicantAddressCollectionEntity, [
            'name' => 'address',
            'type' => ValidationRuleType::TYPE_ENTITY,
            'path' => 'applicant.address',
            'expression' => 'value.house !== 46',
            'mutableGroups' => [],
            'isMutable' => false,
            'errorMessage' => 'Address house should not be equal to 46',
        ]);

        $this->setDataForStoredRules($storedRuleApplicantAddressCollectionEntityTwo, [
            'name' => 'address',
            'type' => ValidationRuleType::TYPE_ENTITY,
            'path' => 'applicant.address',
            'expression' => 'value.flat !== 11',
            'mutableGroups' => [],
            'isMutable' => false,
            'errorMessage' => 'Address flat should not be equal to 11',
        ]);

        $this->setDataForStoredRules($storedRuleApplicantAddressCollectionEntityThree, [
            'name' => 'address',
            'type' => ValidationRuleType::TYPE_ENTITY,
            'path' => 'applicant.address',
            'expression' => 'value.house < 46',
            'mutableGroups' => [],
            'isMutable' => false,
            'errorMessage' => 'Address house number should not be more than 45',
        ]);

        $deal = new Deal();

        $applicantTwo = $this->getMockApplicantWithAddressCollectionDataForValidation();
        $deal->setApplicants([
            'applicantOne' => $this->getMockApplicantWithAddressCollectionDataForValidation(),
            'applicantTwo' => $applicantTwo,
        ]);

        $validationBuilder = (new ValidationBuilder())
            ->addValidationVariable('deal', $deal)
            ->addValidationVariable('applicantId', 'applicantTwo')
            ->addExpression($storedRuleApplicantAddressCollection->getWrappedObject())
            ->addExpression($storedRuleApplicantAddressCollectionEntity->getWrappedObject())
            ->addExpression($storedRuleApplicantAddressCollectionEntityTwo->getWrappedObject())
            ->addExpression($storedRuleApplicantAddressCollectionEntityThree->getWrappedObject())
        ;

        $validationBuilder->build();

        $this->setValidationRules($validationBuilder->getProcessedNestedValidationRules());
        $violations = $this->validate(new ArrayIterator($applicantForValidation->getAddress()), false);
        $this->isValid()->shouldReturn(false);

        $violations->shouldReturnAnInstanceOf(ErrorCollection::class);
        $errorCollection = $violations->getErrorCollection();
        $errorCollection->shouldBeArray();

        //Validate Address error collection structure
        /** @var ErrorCollection $addressErrorCollection */
        $addressErrorCollection =  $errorCollection[0];
        $addressErrorCollection->getName()->shouldBe('address');
        $addressErrorCollection->getErrorCollection()->shouldBeArray();

        //Get errors for the first item entity in the collection
        $addressErrorCollection->getFieldErrorMessages()[ErrorCollection::ENTITY_ERRORS][0]
            ->shouldBe([
                'Address flat should not be equal to 11',
            ]);

        //Get errors for the second item entity in the collection
        $addressErrorCollection->getFieldErrorMessages()[ErrorCollection::ENTITY_ERRORS][1]
            ->shouldBe([
                'Address house should not be equal to 46',
                'Address house number should not be more than 45'
            ]);
    }

    public function it_can_validate_an_invalid_object(
        GeneralRuleInterface $storedRuleApplicant,
        GeneralRuleInterface $storedRuleApplicantName,
        GeneralRuleInterface $storedRuleApplicantType,
        GeneralRuleInterface $storedRuleApplicantAddress,
        GeneralRuleInterface $storedRuleApplicantAddressHouse1,
        GeneralRuleInterface $storedRuleApplicantAddressHouse2,
        GeneralRuleInterface $storedRuleApplicantAddressStreet,
        GeneralRuleInterface $storedRuleApplicantAddressFlat,
        GeneralRuleInterface $storedRuleApplicantAddressMarketing,
        GeneralRuleInterface $storedRuleApplicantAddressMarketingMobileNumber
    ) {
        $applicant = $this->getInvalidMockDataForValidation();

        $validationBuilder = (new ValidationBuilder())
            ->addExpression($storedRuleApplicant->getWrappedObject())
            ->addExpression($storedRuleApplicantName->getWrappedObject())
            ->addExpression($storedRuleApplicantType->getWrappedObject())
            ->addExpression($storedRuleApplicantAddress->getWrappedObject())
            ->addExpression($storedRuleApplicantAddressHouse1->getWrappedObject())
            ->addExpression($storedRuleApplicantAddressHouse2->getWrappedObject())
            ->addExpression($storedRuleApplicantAddressStreet->getWrappedObject())
            ->addExpression($storedRuleApplicantAddressFlat->getWrappedObject())
            ->addExpression($storedRuleApplicantAddressMarketing->getWrappedObject())
            ->addExpression($storedRuleApplicantAddressMarketingMobileNumber->getWrappedObject());

        $validationBuilder->build();

        $this->setValidationRules($validationBuilder->getProcessedNestedValidationRules());

        $violations = $this->validate($applicant);
        $violations->shouldReturnAnInstanceOf(ErrorCollection::class);

        $this->isValid()->shouldReturn(false);
    }

    public function it_can_throw_error_for_is_valid_without_validation(
        GeneralRuleInterface $storedRuleApplicant,
        GeneralRuleInterface $storedRuleApplicantName,
        GeneralRuleInterface $storedRuleApplicantType,
        GeneralRuleInterface $storedRuleApplicantAddress,
        GeneralRuleInterface $storedRuleApplicantAddressHouse1,
        GeneralRuleInterface $storedRuleApplicantAddressHouse2,
        GeneralRuleInterface $storedRuleApplicantAddressStreet,
        GeneralRuleInterface $storedRuleApplicantAddressFlat,
        GeneralRuleInterface $storedRuleApplicantAddressMarketing,
        GeneralRuleInterface $storedRuleApplicantAddressMarketingMobileNumber
    ) {
        $validationBuilder = (new ValidationBuilder())
            ->addExpression($storedRuleApplicant->getWrappedObject())
            ->addExpression($storedRuleApplicantName->getWrappedObject())
            ->addExpression($storedRuleApplicantType->getWrappedObject())
            ->addExpression($storedRuleApplicantAddress->getWrappedObject())
            ->addExpression($storedRuleApplicantAddressHouse1->getWrappedObject())
            ->addExpression($storedRuleApplicantAddressHouse2->getWrappedObject())
            ->addExpression($storedRuleApplicantAddressStreet->getWrappedObject())
            ->addExpression($storedRuleApplicantAddressFlat->getWrappedObject())
            ->addExpression($storedRuleApplicantAddressMarketing->getWrappedObject())
            ->addExpression($storedRuleApplicantAddressMarketingMobileNumber->getWrappedObject());

        $validationBuilder->build();

        $this->setValidationRules($validationBuilder->getProcessedNestedValidationRules());

        $this->shouldThrow(new \InvalidArgumentException('Need to validate an object'))
            ->during('isValid');
    }

    public function it_validates_valid_applicant_when_no_mutable_rules_triggered(
        GeneralRuleInterface $storedRuleApplicant,
        GeneralRuleInterface $storedRuleApplicantName,
        GeneralRuleInterface $storedRuleApplicantType,
        GeneralRuleInterface $storedRuleApplicantAddress,
        GeneralRuleInterface $storedRuleApplicantAddressHouse1,
        GeneralRuleInterface $storedRuleApplicantAddressHouse2,
        GeneralRuleInterface $storedRuleApplicantAddressStreet,
        GeneralRuleInterface $storedRuleApplicantAddressFlat,
        GeneralRuleInterface $storedRuleApplicantAddressMarketing,
        GeneralRuleInterface $storedRuleApplicantAddressMarketingMobileNumber
    ) {
        $applicant = $this->getMockDataForValidation();

        $this->setDataForStoredRules($storedRuleApplicant, [
            'name' => 'applicant',
            'type' => ValidationRuleType::TYPE_ENTITY,
            'path' => 'applicant',
            'expression' => '',
            'mutableGroups' => [],
            'isMutable' => true,
        ]);

        $validationBuilder = (new ValidationBuilder())
            ->addExpression($storedRuleApplicant->getWrappedObject())
            ->addExpression($storedRuleApplicantName->getWrappedObject())
            ->addExpression($storedRuleApplicantType->getWrappedObject())
            ->addExpression($storedRuleApplicantAddress->getWrappedObject())
            ->addExpression($storedRuleApplicantAddressHouse1->getWrappedObject())
            ->addExpression($storedRuleApplicantAddressHouse2->getWrappedObject())
            ->addExpression($storedRuleApplicantAddressStreet->getWrappedObject())
            ->addExpression($storedRuleApplicantAddressFlat->getWrappedObject())
            ->addExpression($storedRuleApplicantAddressMarketing->getWrappedObject())
            ->addExpression($storedRuleApplicantAddressMarketingMobileNumber->getWrappedObject());

        $validationBuilder->build();

        $this->setValidationRules($validationBuilder->getProcessedNestedValidationRules());

        $violations = $this->validate($applicant);
        $violations->shouldReturnAnInstanceOf(ErrorCollection::class);

        $this->isValid()->shouldReturn(true);
    }

    public function it_throws_exception_when_the_overridden_mutable_field_property_does_not_exist(
        GeneralRuleInterface $storedRuleApplicant,
        GeneralRuleInterface $storedRuleApplicantName,
        GeneralRuleInterface $storedRuleApplicantType,
        GeneralRuleInterface $storedRuleApplicantAddress,
        GeneralRuleInterface $storedRuleApplicantAddressHouse1,
        GeneralRuleInterface $storedRuleApplicantAddressHouse2,
        GeneralRuleInterface $storedRuleApplicantAddressStreet,
        GeneralRuleInterface $storedRuleApplicantAddressFlat,
        GeneralRuleInterface $storedRuleApplicantAddressMarketing,
        GeneralRuleInterface $storedRuleApplicantAddressMarketingMobileNumber
    ) {
        $applicant = $this->getMockDataForValidation();

        $this->setDataForStoredRules($storedRuleApplicant, [
            'name' => 'applicant',
            'type' => ValidationRuleType::TYPE_ENTITY,
            'path' => 'applicant',
            'expression' => '',
            'mutableFieldOverride' => 'nonExistingField',
            'mutableGroups' => [],
            'isMutable' => true,
        ]);

        $validationBuilder = (new ValidationBuilder())
            ->addExpression($storedRuleApplicant->getWrappedObject())
            ->addExpression($storedRuleApplicantName->getWrappedObject())
            ->addExpression($storedRuleApplicantType->getWrappedObject())
            ->addExpression($storedRuleApplicantAddress->getWrappedObject())
            ->addExpression($storedRuleApplicantAddressHouse1->getWrappedObject())
            ->addExpression($storedRuleApplicantAddressHouse2->getWrappedObject())
            ->addExpression($storedRuleApplicantAddressStreet->getWrappedObject())
            ->addExpression($storedRuleApplicantAddressFlat->getWrappedObject())
            ->addExpression($storedRuleApplicantAddressMarketing->getWrappedObject())
            ->addExpression($storedRuleApplicantAddressMarketingMobileNumber->getWrappedObject());

        $validationBuilder->build();

        $this->setValidationRules($validationBuilder->getProcessedNestedValidationRules());

        $this->shouldThrow(
            new NoMutatorFoundException(
                sprintf(
                    'No getter found for the object %s and property nonExistingField',
                    get_class($applicant)
                )
            )
        )
            ->during('validate', [$applicant]);
    }

    public function it_validates_valid_applicant_using_mutable_field(
        GeneralRuleInterface $storedRuleApplicant,
        GeneralRuleInterface $storedRuleApplicantName,
        GeneralRuleInterface $storedRuleApplicantType,
        GeneralRuleInterface $storedRuleApplicantAddress,
        GeneralRuleInterface $storedRuleApplicantAddressHouse1,
        GeneralRuleInterface $storedRuleApplicantAddressHouse2,
        GeneralRuleInterface $storedRuleApplicantAddressStreet,
        GeneralRuleInterface $storedRuleApplicantAddressFlat,
        GeneralRuleInterface $storedRuleApplicantAddressMarketing,
        GeneralRuleInterface $storedRuleApplicantAddressMarketingMobileNumber
    ) {
        $applicant = $this->getMockDataForValidation();

        $this->setDataForStoredRules($storedRuleApplicantAddress, [
            'name' => 'address',
            'type' => ValidationRuleType::TYPE_ENTITY,
            'path' => 'applicant.address',
            'expression' => '',
            'mutableFieldOverride' => 'street',
            'mutableGroups' => [],
            'isMutable' => true,
        ]);

        $this->setDataForStoredRules($storedRuleApplicantAddressHouse1, [
            'name' => 'house',
            'type' => ValidationRuleType::TYPE_FIELD,
            'path' => 'applicant.address.house',
            'expression' => 'value > 5',
            'mutableGroups' => ['Marie'],
            'isMutable' => true
        ]);

        $validationBuilder = (new ValidationBuilder())
            ->addExpression($storedRuleApplicant->getWrappedObject())
            ->addExpression($storedRuleApplicantName->getWrappedObject())
            ->addExpression($storedRuleApplicantType->getWrappedObject())
            ->addExpression($storedRuleApplicantAddress->getWrappedObject())
            ->addExpression($storedRuleApplicantAddressHouse1->getWrappedObject())
            ->addExpression($storedRuleApplicantAddressHouse2->getWrappedObject())
            ->addExpression($storedRuleApplicantAddressStreet->getWrappedObject())
            ->addExpression($storedRuleApplicantAddressFlat->getWrappedObject())
            ->addExpression($storedRuleApplicantAddressMarketing->getWrappedObject())
            ->addExpression($storedRuleApplicantAddressMarketingMobileNumber->getWrappedObject());

        $validationBuilder->build();

        $this->setValidationRules($validationBuilder->getProcessedNestedValidationRules());

        $violations = $this->validate($applicant);
        $violations->shouldReturnAnInstanceOf(ErrorCollection::class);

        $this->isValid()->shouldReturn(true);
    }

    public function it_validates_applicant_using_mutable_field_and_skipping_them(
        GeneralRuleInterface $storedRuleApplicant,
        GeneralRuleInterface $storedRuleApplicantName,
        GeneralRuleInterface $storedRuleApplicantType,
        GeneralRuleInterface $storedRuleApplicantAddress,
        GeneralRuleInterface $storedRuleApplicantAddressHouse1,
        GeneralRuleInterface $storedRuleApplicantAddressHouse2,
        GeneralRuleInterface $storedRuleApplicantAddressStreet,
        GeneralRuleInterface $storedRuleApplicantAddressFlat,
        GeneralRuleInterface $storedRuleApplicantAddressMarketing,
        GeneralRuleInterface $storedRuleApplicantAddressMarketingMobileNumber
    ) {
        $applicant = $this->getMockDataForValidation();

        $this->setDataForStoredRules($storedRuleApplicantAddress, [
            'name' => 'address',
            'type' => ValidationRuleType::TYPE_ENTITY,
            'path' => 'applicant.address',
            'expression' => '',
            'mutableFieldOverride' => 'street',
            'mutableGroups' => [],
            'isMutable' => true,
        ]);

        // this invalid rule should not be applied because it is not in the mutable group
        $this->setDataForStoredRules($storedRuleApplicantAddressHouse1, [
            'name' => 'house',
            'type' => ValidationRuleType::TYPE_FIELD,
            'path' => 'applicant.address.house',
            'expression' => 'value < 5',
            'mutableGroups' => ['Not Marie'],
            'isMutable' => true
        ]);

        // this invalid rule should also be skipped because it is not in the mutable group
        $this->setDataForStoredRules($storedRuleApplicantAddressFlat, [
            'name' => 'flat',
            'type' => ValidationRuleType::TYPE_FIELD,
            'path' => 'applicant.address.flat',
            'expression' => 'value > 100',
            'mutableGroups' => ['Not Marie Again'],
            'isMutable' => true
        ]);

        $validationBuilder = (new ValidationBuilder())
            ->addExpression($storedRuleApplicant->getWrappedObject())
            ->addExpression($storedRuleApplicantName->getWrappedObject())
            ->addExpression($storedRuleApplicantType->getWrappedObject())
            ->addExpression($storedRuleApplicantAddress->getWrappedObject())
            ->addExpression($storedRuleApplicantAddressHouse1->getWrappedObject())
            ->addExpression($storedRuleApplicantAddressHouse2->getWrappedObject())
            ->addExpression($storedRuleApplicantAddressStreet->getWrappedObject())
            ->addExpression($storedRuleApplicantAddressFlat->getWrappedObject())
            ->addExpression($storedRuleApplicantAddressMarketing->getWrappedObject())
            ->addExpression($storedRuleApplicantAddressMarketingMobileNumber->getWrappedObject());

        $validationBuilder->build();

        $this->setValidationRules($validationBuilder->getProcessedNestedValidationRules());

        $violations = $this->validate($applicant);
        $violations->shouldReturnAnInstanceOf(ErrorCollection::class);

        $this->isValid()->shouldReturn(true);
    }

    public function it_validates_valid_applicant_house_with_mutable_street_and_several_rules_for_house(
        GeneralRuleInterface $storedRuleApplicant,
        GeneralRuleInterface $storedRuleApplicantName,
        GeneralRuleInterface $storedRuleApplicantType,
        GeneralRuleInterface $storedRuleApplicantAddress,
        GeneralRuleInterface $storedRuleApplicantAddressHouse1,
        GeneralRuleInterface $storedRuleApplicantAddressHouse2,
        GeneralRuleInterface $storedRuleApplicantAddressStreet,
        GeneralRuleInterface $storedRuleApplicantAddressFlat,
        GeneralRuleInterface $storedRuleApplicantAddressMarketing,
        GeneralRuleInterface $storedRuleApplicantAddressMarketingMobileNumber
    ) {
        $applicant = $this->getMockDataForValidation();

        $this->setDataForStoredRules($storedRuleApplicantAddress, [
            'name' => 'address',
            'type' => ValidationRuleType::TYPE_ENTITY,
            'path' => 'applicant.address',
            'expression' => '',
            'mutableFieldOverride' => 'street',
            'mutableGroups' => [],
            'isMutable' => true,
        ]);

        // this rule should be skipped
        $this->setDataForStoredRules($storedRuleApplicantAddressHouse1, [
            'name' => 'house',
            'type' => ValidationRuleType::TYPE_FIELD,
            'path' => 'applicant.address.house',
            'expression' => 'value < 5',
            'mutableGroups' => ['Not Marie'],
            'isMutable' => true
        ]);

        // this rule should be applied
        $this->setDataForStoredRules($storedRuleApplicantAddressHouse2, [
            'name' => 'house',
            'type' => ValidationRuleType::TYPE_FIELD,
            'path' => 'applicant.address.house',
            'expression' => 'value > 5',
            'mutableGroups' => ['Marie'],
            'isMutable' => true
        ]);

        $validationBuilder = (new ValidationBuilder())
            ->addExpression($storedRuleApplicant->getWrappedObject())
            ->addExpression($storedRuleApplicantName->getWrappedObject())
            ->addExpression($storedRuleApplicantType->getWrappedObject())
            ->addExpression($storedRuleApplicantAddress->getWrappedObject())
            ->addExpression($storedRuleApplicantAddressHouse1->getWrappedObject())
            ->addExpression($storedRuleApplicantAddressHouse2->getWrappedObject())
            ->addExpression($storedRuleApplicantAddressStreet->getWrappedObject())
            ->addExpression($storedRuleApplicantAddressFlat->getWrappedObject())
            ->addExpression($storedRuleApplicantAddressMarketing->getWrappedObject())
            ->addExpression($storedRuleApplicantAddressMarketingMobileNumber->getWrappedObject());

        $validationBuilder->build();

        $this->setValidationRules($validationBuilder->getProcessedNestedValidationRules());

        $violations = $this->validate($applicant);
        $violations->shouldReturnAnInstanceOf(ErrorCollection::class);

        $this->isValid()->shouldReturn(true);
    }

    public function it_validates_invalid_applicant_house_with_mutable_street_and_several_rules_for_house(
        GeneralRuleInterface $storedRuleApplicant,
        GeneralRuleInterface $storedRuleApplicantName,
        GeneralRuleInterface $storedRuleApplicantType,
        GeneralRuleInterface $storedRuleApplicantAddress,
        GeneralRuleInterface $storedRuleApplicantAddressHouse1,
        GeneralRuleInterface $storedRuleApplicantAddressHouse2,
        GeneralRuleInterface $storedRuleApplicantAddressStreet,
        GeneralRuleInterface $storedRuleApplicantAddressFlat,
        GeneralRuleInterface $storedRuleApplicantAddressMarketing,
        GeneralRuleInterface $storedRuleApplicantAddressMarketingMobileNumber
    ) {
        $applicant = $this->getMockDataForValidation();

        $this->setDataForStoredRules($storedRuleApplicantAddress, [
            'name' => 'address',
            'type' => ValidationRuleType::TYPE_ENTITY,
            'path' => 'applicant.address',
            'expression' => '',
            'mutableFieldOverride' => 'street',
            'mutableGroups' => [],
            'isMutable' => true,
        ]);

        // this rule should be skipped
        $this->setDataForStoredRules($storedRuleApplicantAddressHouse1, [
            'name' => 'house',
            'type' => ValidationRuleType::TYPE_FIELD,
            'path' => 'applicant.address.house',
            'expression' => 'value < 5',
            'mutableGroups' => ['Marie'],
            'isMutable' => true
        ]);

        // this rule should be applied
        $this->setDataForStoredRules($storedRuleApplicantAddressHouse2, [
            'name' => 'house',
            'type' => ValidationRuleType::TYPE_FIELD,
            'path' => 'applicant.address.house',
            'expression' => 'value > 5',
            'mutableGroups' => ['Not Marie'],
            'isMutable' => true
        ]);

        $validationBuilder = (new ValidationBuilder())
            ->addExpression($storedRuleApplicant->getWrappedObject())
            ->addExpression($storedRuleApplicantName->getWrappedObject())
            ->addExpression($storedRuleApplicantType->getWrappedObject())
            ->addExpression($storedRuleApplicantAddress->getWrappedObject())
            ->addExpression($storedRuleApplicantAddressHouse1->getWrappedObject())
            ->addExpression($storedRuleApplicantAddressHouse2->getWrappedObject())
            ->addExpression($storedRuleApplicantAddressStreet->getWrappedObject())
            ->addExpression($storedRuleApplicantAddressFlat->getWrappedObject())
            ->addExpression($storedRuleApplicantAddressMarketing->getWrappedObject())
            ->addExpression($storedRuleApplicantAddressMarketingMobileNumber->getWrappedObject());

        $validationBuilder->build();

        $this->setValidationRules($validationBuilder->getProcessedNestedValidationRules());

        $violations = $this->validate($applicant);
        $violations->shouldReturnAnInstanceOf(ErrorCollection::class);

        $this->isValid()->shouldReturn(false);
        $this->getAmountOfErroneousFields()->shouldReturn(1);
    }

    public function it_validates_invalid_applicant_using_mutable_field(
        GeneralRuleInterface $storedRuleApplicant,
        GeneralRuleInterface $storedRuleApplicantName,
        GeneralRuleInterface $storedRuleApplicantType,
        GeneralRuleInterface $storedRuleApplicantAddress,
        GeneralRuleInterface $storedRuleApplicantAddressHouse1,
        GeneralRuleInterface $storedRuleApplicantAddressHouse2,
        GeneralRuleInterface $storedRuleApplicantAddressStreet,
        GeneralRuleInterface $storedRuleApplicantAddressFlat,
        GeneralRuleInterface $storedRuleApplicantAddressMarketing,
        GeneralRuleInterface $storedRuleApplicantAddressMarketingMobileNumber
    ) {
        $applicant = $this->getMockDataForValidation();

        $this->setDataForStoredRules($storedRuleApplicantAddress, [
            'name' => 'address',
            'type' => ValidationRuleType::TYPE_ENTITY,
            'path' => 'applicant.address',
            'expression' => '',
            'mutableFieldOverride' => 'street',
            'mutableGroups' => [],
            'isMutable' => true,
        ]);

        $this->setDataForStoredRules($storedRuleApplicantAddressHouse1, [
            'name' => 'house',
            'type' => ValidationRuleType::TYPE_FIELD,
            'path' => 'applicant.address.house',
            'expression' => 'value < 5',
            'mutableGroups' => ['Marie'],
            'isMutable' => true
        ]);

        $this->setDataForStoredRules($storedRuleApplicantAddressFlat, [
            'name' => 'flat',
            'type' => ValidationRuleType::TYPE_FIELD,
            'path' => 'applicant.address.flat',
            'expression' => 'value > 100',
            'mutableGroups' => ['Marie'],
            'isMutable' => true
        ]);

        $validationBuilder = (new ValidationBuilder())
            ->addExpression($storedRuleApplicant->getWrappedObject())
            ->addExpression($storedRuleApplicantName->getWrappedObject())
            ->addExpression($storedRuleApplicantType->getWrappedObject())
            ->addExpression($storedRuleApplicantAddress->getWrappedObject())
            ->addExpression($storedRuleApplicantAddressHouse1->getWrappedObject())
            ->addExpression($storedRuleApplicantAddressHouse2->getWrappedObject())
            ->addExpression($storedRuleApplicantAddressStreet->getWrappedObject())
            ->addExpression($storedRuleApplicantAddressFlat->getWrappedObject())
            ->addExpression($storedRuleApplicantAddressMarketing->getWrappedObject())
            ->addExpression($storedRuleApplicantAddressMarketingMobileNumber->getWrappedObject());

        $validationBuilder->build();

        $this->setValidationRules($validationBuilder->getProcessedNestedValidationRules());

        $violations = $this->validate($applicant);
        $violations->shouldReturnAnInstanceOf(ErrorCollection::class);

        $this->isValid()->shouldReturn(false);
        $this->getAmountOfErroneousFields()->shouldReturn(2);
    }

    function it_can_validate_collections_with_parent_access(
        GeneralRuleInterface $storedRuleApplicant,
        GeneralRuleInterface $storedRuleApplicantAddress
    ) {
        $this->setDataForStoredRules($storedRuleApplicant, [
            'name' => 'applicant',
            'type' => ValidationRuleType::TYPE_COLLECTION,
            'path' => 'applicants',
            'expression' => '',
            'mutableGroups' => [],
            'isMutable' => false,
        ]);

        $this->setDataForStoredRules($storedRuleApplicantAddress, [
            'name' => 'address',
            'type' => ValidationRuleType::TYPE_ENTITY,
            'path' => 'applicants.address',
            'expression' => '(parent.type.name === "Marie" and value !== null) or (parent.type.name !== "Marie")',
            'mutableGroups' => [],
            'isMutable' => false,
        ]);

        $applicantOne = $this->getMockDataForValidation();
        $applicantTwo = $this->getMockDataForValidation();
        $applicantTwo->setType(ApplicantType::Marie);

        $validationBuilder = (new ValidationBuilder())
            ->addExpression($storedRuleApplicant->getWrappedObject())
            ->addExpression($storedRuleApplicantAddress->getWrappedObject());

        $validationBuilder->build();

        $this->setValidationRules($validationBuilder->getProcessedNestedValidationRules());
        $violations = $this->validate(new ArrayIterator([$applicantOne, $applicantTwo]), false);

        $violations->shouldReturnAnInstanceOf(ErrorCollection::class);

        $this->isValid()->shouldReturn(true);
    }

    function it_can_validate_invalid_collections_with_parent_access(
        GeneralRuleInterface $storedRuleApplicant,
        GeneralRuleInterface $storedRuleApplicantAddress
    ) {
        $this->setDataForStoredRules($storedRuleApplicant, [
            'name' => 'applicant',
            'type' => ValidationRuleType::TYPE_COLLECTION,
            'path' => 'applicants',
            'expression' => '',
            'mutableGroups' => [],
            'isMutable' => false,
        ]);

        $this->setDataForStoredRules($storedRuleApplicantAddress, [
            'name' => 'address',
            'type' => ValidationRuleType::TYPE_ENTITY,
            'path' => 'applicants.address',
            'expression' => '(parent.type.name === "Marie" and value !== null) or (parent.type.name !== "Marie")',
            'mutableGroups' => [],
            'isMutable' => false,
        ]);

        $applicantOne = $this->getMockDataForValidation();
        $applicantTwo = $this->getMockDataForValidation();
        $applicantTwo->setType(ApplicantType::Marie);
        $applicantTwo->setAddress(null);

        $validationBuilder = (new ValidationBuilder())
            ->addExpression($storedRuleApplicant->getWrappedObject())
            ->addExpression($storedRuleApplicantAddress->getWrappedObject());

        $validationBuilder->build();

        $this->setValidationRules($validationBuilder->getProcessedNestedValidationRules());
        $violations = $this->validate(new ArrayIterator([$applicantOne, $applicantTwo]), false);

        $violations->shouldReturnAnInstanceOf(ErrorCollection::class);

        $this->isValid()->shouldReturn(false);
    }

    function it_can_validate_invalid_object_with_parent_access(
        GeneralRuleInterface $storedRuleApplicant,
        GeneralRuleInterface $storedRuleApplicantAddress
    ) {
        $this->setDataForStoredRules($storedRuleApplicantAddress, [
            'name' => 'address',
            'type' => ValidationRuleType::TYPE_ENTITY,
            'path' => 'applicant.address',
            'expression' => '(parent.type.name === "Marie" and value !== null) or (parent.type.name !== "Marie")',
            'mutableGroups' => [],
            'isMutable' => false,
        ]);

        $applicant = $this->getMockDataForValidation();
        $applicant->setType(ApplicantType::Marie);
        $applicant->setAddress(null);

        $validationBuilder = (new ValidationBuilder())
            ->addExpression($storedRuleApplicant->getWrappedObject())
            ->addExpression($storedRuleApplicantAddress->getWrappedObject());

        $validationBuilder->build();

        $this->setValidationRules($validationBuilder->getProcessedNestedValidationRules());
        $violations = $this->validate($applicant);

        $violations->shouldReturnAnInstanceOf(ErrorCollection::class);

        $this->isValid()->shouldReturn(false);
    }

    function it_can_validate_valid_object_with_parent_access(
        GeneralRuleInterface $storedRuleApplicant,
        GeneralRuleInterface $storedRuleApplicantAddress
    ) {
        $this->setDataForStoredRules($storedRuleApplicantAddress, [
            'name' => 'address',
            'type' => ValidationRuleType::TYPE_ENTITY,
            'path' => 'applicant.address',
            'expression' => '(parent.type.name === "Marie" and value !== null) or (parent.type.name !== "Marie")',
            'mutableGroups' => [],
            'isMutable' => false,
        ]);

        $applicant = $this->getMockDataForValidation();
        $applicant->setType(ApplicantType::Marie);

        $validationBuilder = (new ValidationBuilder())
            ->addExpression($storedRuleApplicant->getWrappedObject())
            ->addExpression($storedRuleApplicantAddress->getWrappedObject());

        $validationBuilder->build();

        $this->setValidationRules($validationBuilder->getProcessedNestedValidationRules());
        $violations = $this->validate($applicant);

        $violations->shouldReturnAnInstanceOf(ErrorCollection::class);

        $this->isValid()->shouldReturn(true);
    }

    function it_can_validate_invalid_field_with_parent_access(
        GeneralRuleInterface $storedRuleApplicant,
        GeneralRuleInterface $storedRuleApplicantAddress,
        GeneralRuleInterface $storedRuleApplicantAddressStreet
    ) {
        $this->setDataForStoredRules($storedRuleApplicantAddressStreet, [
            'name' => 'street',
            'type' => ValidationRuleType::TYPE_FIELD,
            'path' => 'applicant.address.street',
            'expression' => '(parent.type.name === "Marie" and value !== null) or (parent.type.name !== "Marie")',
            'mutableGroups' => [],
            'isMutable' => false,
        ]);

        $applicant = $this->getMockDataForValidation();
        $applicant->setType(ApplicantType::Marie);
        $applicant->getAddress()->setStreet(null);

        $validationBuilder = (new ValidationBuilder())
            ->addExpression($storedRuleApplicant->getWrappedObject())
            ->addExpression($storedRuleApplicantAddress->getWrappedObject())
            ->addExpression($storedRuleApplicantAddressStreet->getWrappedObject());

        $validationBuilder->build();

        $this->setValidationRules($validationBuilder->getProcessedNestedValidationRules());
        $violations = $this->validate($applicant);

        $violations->shouldReturnAnInstanceOf(ErrorCollection::class);

        $this->isValid()->shouldReturn(false);
    }

    function it_can_validate_valid_field_with_parent_access(
        GeneralRuleInterface $storedRuleApplicant,
        GeneralRuleInterface $storedRuleApplicantAddress,
        GeneralRuleInterface $storedRuleApplicantAddressStreet
    ) {
        $this->setDataForStoredRules($storedRuleApplicantAddressStreet, [
            'name' => 'street',
            'type' => ValidationRuleType::TYPE_FIELD,
            'path' => 'applicant.address.street',
            'expression' => '(parent.type.name === "Marie" and value !== null) or (parent.type.name !== "Marie")',
            'mutableGroups' => [],
            'isMutable' => false,
        ]);

        $applicant = $this->getMockDataForValidation();
        $applicant->setType(ApplicantType::Marie);

        $validationBuilder = (new ValidationBuilder())
            ->addExpression($storedRuleApplicant->getWrappedObject())
            ->addExpression($storedRuleApplicantAddress->getWrappedObject())
            ->addExpression($storedRuleApplicantAddressStreet->getWrappedObject());

        $validationBuilder->build();

        $this->setValidationRules($validationBuilder->getProcessedNestedValidationRules());
        $violations = $this->validate($applicant);

        $violations->shouldReturnAnInstanceOf(ErrorCollection::class);

        $this->isValid()->shouldReturn(true);
    }

    function it_can_validate_null_entity_with_child_rules (
        GeneralRuleInterface $storedRuleApplicant,
        GeneralRuleInterface $storedRuleApplicantAddress,
        GeneralRuleInterface $storedRuleApplicantAddressStreet
    ) {
        $this->setDataForStoredRules($storedRuleApplicantAddress, [
            'name' => 'address',
            'type' => ValidationRuleType::TYPE_ENTITY,
            'path' => 'applicant.address',
            'expression' => '',
            'mutableGroups' => [],
            'isMutable' => true,
        ]);

        $applicant = $this->getMockDataForValidation();
        $applicant->setType(ApplicantType::Marie);
        $applicant->setAddress(null);

        $validationBuilder = (new ValidationBuilder())
            ->addExpression($storedRuleApplicant->getWrappedObject())
            ->addExpression($storedRuleApplicantAddress->getWrappedObject())
            ->addExpression($storedRuleApplicantAddressStreet->getWrappedObject());

        $validationBuilder->build();

        $this->setValidationRules($validationBuilder->getProcessedNestedValidationRules());
        $violations = $this->validate($applicant);

        $violations->shouldReturnAnInstanceOf(ErrorCollection::class);

        $this->isValid()->shouldReturn(true);
    }

    function it_can_validate_enum_type(
        GeneralRuleInterface $storedRuleApplicant,
        GeneralRuleInterface $storedRuleApplicantAddress,
        GeneralRuleInterface $storedRuleApplicantAddressStreet,
        GeneralRuleInterface $storedRuleApplicantType,
    ) {
        $this->setDataForStoredRules($storedRuleApplicantType, [
            'name' => 'type',
            'type' => ValidationRuleType::TYPE_FIELD,
            'path' => 'applicant.type',
            'expression' => 'value in allowedValues',
            'mutableGroups' => [],
            'isMutable' => false
        ]);

        $applicant = $this->getMockDataForValidation();
        $applicant->setType(ApplicantType::Marie);
        $applicant->setAddress(null);

        $validationBuilder = (new ValidationBuilder())
            ->addExpression($storedRuleApplicant->getWrappedObject())
            ->addExpression($storedRuleApplicantType->getWrappedObject())
            ->addExpression($storedRuleApplicantAddress->getWrappedObject())
            ->addExpression($storedRuleApplicantAddressStreet->getWrappedObject())
            ->addValidationVariable('allowedValues', [ApplicantType::Marie, ApplicantType::Good]);

        $validationBuilder->build();

        $this->setValidationRules($validationBuilder->getProcessedNestedValidationRules());
        $violations = $this->validate($applicant);

        $violations->shouldReturnAnInstanceOf(ErrorCollection::class);

        $this->isValid()->shouldReturn(true);
    }

    /**
     * @param ValidationRuleInterface $validationRule
     * @param GeneralRuleInterface $storedRule
     * @param array $data
     */
    protected function setDataForValidationRules(
        ValidationRuleInterface $validationRule,
        GeneralRuleInterface $storedRule,
        array $data
    ) {
        $validationRule->getName()->willReturn($data['name']);
        $validationRule->getType()->willReturn($data['type']);
        $validationRule->getPath()->willReturn($data['path']);
        $validationRule->getExpression()->willReturn($data['expression']);
        $validationRule->getMutableGroups()->willReturn($data['mutableGroups']);
        $validationRule->isMutable()->willReturn($data['isMutable']);
        $validationRule->getRules()->willReturn([$storedRule]);
    }

    /**
     * @param GeneralRuleInterface $storedRule
     * @param array $data
     */
    protected function setDataForStoredRules(GeneralRuleInterface $storedRule, array $data)
    {
        $storedRule->getName()->willReturn($data['name']);
        $storedRule->getType()->willReturn($data['type']);
        $storedRule->getPath()->willReturn($data['path']);
        $storedRule->getExpression()->willReturn($data['expression']);
        $storedRule->getErrorMessage()->willReturn($data['errorMessage'] ?? '');
        $storedRule->getMutableGroups()->willReturn($data['mutableGroups']);
        $storedRule->isMutable()->willReturn($data['isMutable']);
        $storedRule->getCollectionIndexName()->willReturn($data['collectionIndexName'] ?? '');
        $storedRule->getMutableFieldOverride()->willReturn($data['mutableFieldOverride'] ?? null);
    }

    /**
     * @return Applicant
     */
    protected function getMockDataForValidation(): Applicant
    {
        $marketing = new Marketing();
        $marketing->setMobileNumber('134134325');

        $address = new Address();
        $address->setFlat(11);
        $address->setHouse(45);
        $address->setStreet('Marie');
        $address->setMarketing($marketing);

        $applicant = new Applicant();
        $applicant->setName('Andy');
        $applicant->setType(ApplicantType::Good);
        $applicant->setAddress($address);

        return $applicant;
    }

    /**
     * @return Applicant
     */
    protected function getMockDataForValidationWithPropertyGetter(): Applicant
    {
        $marketing = new Marketing();
        $marketing->setMobileNumber('134134325');

        $address = new Address();
        $address->setFlat(11);
        $address->setHouse(78);
        $address->setStreet('Marie');
        $address->setMarketing($marketing);

        $applicant = new Applicant();
        $applicant->setName('Andy');
        $applicant->setType(ApplicantType::Good);
        $applicant->setAddress($address);

        return $applicant;
    }

    /**
     * @return Applicant
     */
    protected function getInvalidMockDataForValidation(): Applicant
    {
        $marketing = new Marketing();
        $marketing->setMobileNumber('1');

        $address = new Address();
        $address->setFlat(11);
        $address->setHouse(45);
        $address->setStreet('Marie');
        $address->setMarketing($marketing);

        $applicant = new Applicant();
        $applicant->setName('Andy');
        $applicant->setType(ApplicantType::Good1);
        $applicant->setAddress($address);

        return $applicant;
    }

    /**
     * @return ApplicantWithAddressCollection
     */
    protected function getMockApplicantWithAddressCollectionDataForValidation(): ApplicantWithAddressCollection
    {
        $marketing1 = new Marketing();
        $marketing1->setMobileNumber('134134325');

        $marketing2 = new Marketing();
        $marketing2->setMobileNumber('5677790');

        $address1 = new Address();
        $address1->setFlat(11);
        $address1->setHouse(45);
        $address1->setStreet('Marie');
        $address1->setMarketing($marketing1);

        $address2 = new Address();
        $address2->setFlat(29);
        $address2->setHouse(46);
        $address2->setStreet('Marie');
        $address2->setMarketing($marketing2);

        $applicant = new ApplicantWithAddressCollection();
        $applicant->setName('Andy');
        $applicant->setType(ApplicantType::Good);
        $applicant->setAddress([$address1,$address2]);

        return $applicant;
    }

    function let(
        GeneralRuleInterface $storedRuleApplicant,
        GeneralRuleInterface $storedRuleApplicantName,
        GeneralRuleInterface $storedRuleApplicantType,
        GeneralRuleInterface $storedRuleApplicantAddress,
        GeneralRuleInterface $storedRuleApplicantAddressHouse1,
        GeneralRuleInterface $storedRuleApplicantAddressHouse2,
        GeneralRuleInterface $storedRuleApplicantAddressStreet,
        GeneralRuleInterface $storedRuleApplicantAddressFlat,
        GeneralRuleInterface $storedRuleApplicantAddressMarketing,
        GeneralRuleInterface $storedRuleApplicantAddressMarketingMobileNumber
    ) {
        $this->setDataForStoredRules($storedRuleApplicant, [
            'name' => 'applicant',
            'type' => ValidationRuleType::TYPE_ENTITY,
            'path' => 'applicant',
            'expression' => '',
            'mutableGroups' => [],
            'isMutable' => false,
        ]);

        $this->setDataForStoredRules($storedRuleApplicantName, [
            'name' => 'name',
            'type' => ValidationRuleType::TYPE_FIELD,
            'path' => 'applicant.name',
            'expression' => 'this.getType() != value',
            'mutableGroups' => [],
            'isMutable' => false
        ]);

        $this->setDataForStoredRules($storedRuleApplicantType, [
            'name' => 'type',
            'type' => ValidationRuleType::TYPE_FIELD,
            'path' => 'applicant.type',
            'expression' => 'value.name in ["Good", "Better"]',
            'mutableGroups' => [],
            'isMutable' => false
        ]);

        $this->setDataForStoredRules($storedRuleApplicantAddress, [
            'name' => 'address',
            'type' => ValidationRuleType::TYPE_ENTITY,
            'path' => 'applicant.address',
            'expression' => '',
            'mutableGroups' => [],
            'isMutable' => false,
        ]);

        $this->setDataForStoredRules($storedRuleApplicantAddressHouse1, [
            'name' => 'house',
            'type' => ValidationRuleType::TYPE_FIELD,
            'path' => 'applicant.address.house',
            'expression' => 'value > 5',
            'mutableGroups' => [],
            'isMutable' => false
        ]);

        $this->setDataForStoredRules($storedRuleApplicantAddressHouse2, [
            'name' => 'house',
            'type' => ValidationRuleType::TYPE_FIELD,
            'path' => 'applicant.address.house',
            'expression' => 'value < 50',
            'mutableGroups' => [],
            'isMutable' => false
        ]);

        $this->setDataForStoredRules($storedRuleApplicantAddressStreet, [
            'name' => 'street',
            'type' => ValidationRuleType::TYPE_FIELD,
            'path' => 'applicant.address.street',
            'expression' => 'value in ["Marie", "Victoria"]',
            'mutableGroups' => [],
            'isMutable' => false
        ]);

        $this->setDataForStoredRules($storedRuleApplicantAddressFlat, [
            'name' => 'flat',
            'type' => ValidationRuleType::TYPE_FIELD,
            'path' => 'applicant.address.flat',
            'expression' => 'value < 30',
            'mutableGroups' => [],
            'isMutable' => false
        ]);

        $this->setDataForStoredRules($storedRuleApplicantAddressMarketing, [
            'name' => 'marketing',
            'type' => ValidationRuleType::TYPE_ENTITY,
            'path' => 'applicant.address.marketing',
            'expression' => '',
            'mutableGroups' => [],
            'isMutable' => false,
        ]);

        $this->setDataForStoredRules($storedRuleApplicantAddressMarketingMobileNumber, [
            'name' => 'mobileNumber',
            'type' => ValidationRuleType::TYPE_FIELD,
            'path' => 'applicant.address.marketing.mobileNumber',
            'expression' => 'value > 1000',
            'mutableGroups' => [],
            'isMutable' => false,
        ]);
    }
}
