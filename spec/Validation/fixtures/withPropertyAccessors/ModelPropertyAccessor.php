<?php


namespace spec\DealTrak\Validator\Validation\fixtures\withPropertyAccessors;

class ModelPropertyAccessor
{

    /**
     * This is useful for accessing fields using their getters
     *=
     * @param $name
     *
     * @return mixed
     */
    public function __get(string $name)
    {
        $getterMethod = 'get' . ucfirst($name);

        if (method_exists($this, $getterMethod)) {
            return $this->$getterMethod();
        }

        $getterMethod = 'is' . ucfirst($name);

        if (method_exists($this, $getterMethod)) {
            return $this->$getterMethod();
        }
    }
}
