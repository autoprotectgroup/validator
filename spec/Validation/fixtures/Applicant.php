<?php


namespace spec\DealTrak\Validator\Validation\fixtures;

use spec\DealTrak\Validator\Validation\fixtures\withPropertyAccessors\ModelPropertyAccessor;

class Applicant extends ModelPropertyAccessor
{
    protected string $name;

    protected ApplicantType $type;

    protected ?Address $address;

    protected array $marketingCollection;

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @param string $name
     */
    public function setName(string $name): void
    {
        $this->name = $name;
    }

    /**
     * @return ApplicantType
     */
    public function getType(): ApplicantType
    {
        return $this->type;
    }

    /**
     * @param ApplicantType $type
     */
    public function setType(ApplicantType $type): void
    {
        $this->type = $type;
    }

    /**
     * @return Address
     */
    public function getAddress(): ?Address
    {
        return $this->address;
    }

    /**
     * @param Address $address
     */
    public function setAddress(?Address $address): void
    {
        $this->address = $address;
    }

    /**
     * @return array
     */
    public function getMarketingCollection(): array
    {
        return $this->marketingCollection;
    }

    /**
     * @param array $marketingCollection
     */
    public function setMarketingCollection(array $marketingCollection): void
    {
        $this->marketingCollection = $marketingCollection;
    }


}

