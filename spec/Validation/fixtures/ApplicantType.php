<?php

declare(strict_types=1);

namespace spec\DealTrak\Validator\Validation\fixtures;

enum ApplicantType
{
    case Good1;
    case Good;
    case Marie;
}
