<?php


namespace spec\DealTrak\Validator\Validation\fixtures;

use spec\DealTrak\Validator\Validation\fixtures\withPropertyAccessors\ModelPropertyAccessor;

class Address extends ModelPropertyAccessor
{
    protected int $house;

    protected int $flat;

    protected ?string $street;

    protected Marketing $marketing;

    /**
     * @return int
     */
    public function getHouse(): int
    {
        return $this->house;
    }

    /**
     * @param int $house
     */
    public function setHouse(int $house): void
    {
        $this->house = $house;
    }

    /**
     * @return int
     */
    public function getFlat(): int
    {
        return $this->flat;
    }

    /**
     * @param int $flat
     */
    public function setFlat(int $flat): void
    {
        $this->flat = $flat;
    }

    /**
     * @return string
     */
    public function getStreet(): ?string
    {
        return $this->street;
    }

    /**
     * @param string $street
     */
    public function setStreet(?string $street): void
    {
        $this->street = $street;
    }

    /**
     * @return Marketing
     */
    public function getMarketing(): Marketing
    {
        return $this->marketing;
    }

    /**
     * @param Marketing $marketing
     */
    public function setMarketing(Marketing $marketing): void
    {
        $this->marketing = $marketing;
    }
}
