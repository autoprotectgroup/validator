<?php

declare(strict_types=1);

namespace spec\DealTrak\Validator\Validation\fixtures\validationExpressions\enums;

enum EnumBackedString: string
{
    case NAME_ONE = 'Name one';
    case NAME_TWO = 'Name two';
}
