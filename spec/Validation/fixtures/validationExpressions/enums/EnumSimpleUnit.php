<?php

declare(strict_types=1);

namespace spec\DealTrak\Validator\Validation\fixtures\validationExpressions\enums;

enum EnumSimpleUnit
{
    case NAME_ONE;
    case NAME_TWO;
}
