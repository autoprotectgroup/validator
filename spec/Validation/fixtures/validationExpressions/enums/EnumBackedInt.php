<?php

declare(strict_types=1);

namespace spec\DealTrak\Validator\Validation\fixtures\validationExpressions\enums;

enum EnumBackedInt: int
{
    case NAME_ONE = 1;
    case NAME_TWO = 2;
}
