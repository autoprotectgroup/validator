<?php


namespace spec\DealTrak\Validator\Validation\fixtures;

use spec\DealTrak\Validator\Validation\fixtures\withPropertyAccessors\ModelPropertyAccessor;

class Marketing extends ModelPropertyAccessor
{
    protected string $mobileNumber;

    /**
     * @return string
     */
    public function getMobileNumber(): string
    {
        return $this->mobileNumber;
    }

    /**
     * @param string $mobileNumber
     */
    public function setMobileNumber(string $mobileNumber): void
    {
        $this->mobileNumber = $mobileNumber;
    }
}
