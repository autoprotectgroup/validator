<?php


namespace spec\DealTrak\Validator\Validation\fixtures\validWithGlobalVariable;

use spec\DealTrak\Validator\Validation\fixtures\Applicant;
use spec\DealTrak\Validator\Validation\fixtures\withPropertyAccessors\ModelPropertyAccessor;

class Deal extends ModelPropertyAccessor
{

    protected string $applicantType;

    /** @var Applicant[] */
    protected array $applicants;

    /**
     * @return string
     */
    public function getApplicantType(): string
    {
        return $this->applicantType;
    }

    /**
     * @param string $applicantType
     */
    public function setApplicantType(string $applicantType): void
    {
        $this->applicantType = $applicantType;
    }

    /**
     * @return array
     */
    public function getApplicants(): array
    {
        return $this->applicants;
    }

    /**
     * @param array $applicants
     */
    public function setApplicants(array $applicants): void
    {
        $this->applicants = $applicants;
    }
}
