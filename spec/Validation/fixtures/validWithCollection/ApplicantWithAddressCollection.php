<?php


namespace spec\DealTrak\Validator\Validation\fixtures\validWithCollection;

use spec\DealTrak\Validator\Validation\fixtures\Address;
use spec\DealTrak\Validator\Validation\fixtures\ApplicantType;

class ApplicantWithAddressCollection
{
    protected string $name;

    protected ApplicantType $type;

    /** @var Address[]  */
    protected array $address;

    protected array $marketingCollection;

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @param string $name
     */
    public function setName(string $name): void
    {
        $this->name = $name;
    }

    /**
     * @return ApplicantType
     */
    public function getType(): ApplicantType
    {
        return $this->type;
    }

    /**
     * @param ApplicantType $type
     */
    public function setType(ApplicantType $type): void
    {
        $this->type = $type;
    }

    /**
     * @return array
     */
    public function getAddress(): array
    {
        return $this->address;
    }

    /**
     * @param array $address
     */
    public function setAddress(array $address): void
    {
        $this->address = $address;
    }

    /**
     * @return array
     */
    public function getMarketingCollection(): array
    {
        return $this->marketingCollection;
    }

    /**
     * @param array $marketingCollection
     */
    public function setMarketingCollection(array $marketingCollection): void
    {
        $this->marketingCollection = $marketingCollection;
    }


}

