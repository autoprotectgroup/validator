<?php

namespace spec\DealTrak\Validator\Validation\Serializer;

use DealTrak\Validator\Validation\Error\ErrorCollection;
use DealTrak\Validator\Validation\Serializer\ErrorCollectionSerializer;
use PhpSpec\ObjectBehavior;

/**
 * Class ErrorCollectionSerializerSpec
 *
 * @package spec\DealTrak\Dealer\Validation\Serializer
 *
 * @codingStandardsIgnoreFile
 */
class ErrorCollectionSerializerSpec extends ObjectBehavior
{
    function it_is_initializable(): void
    {
        $this->shouldHaveType(ErrorCollectionSerializer::class);
    }

    /**
     * It will check serialization output by given violation list
     */
    function it_can_serialize_violation_error_list(): void
    {
        $this
            ->serialize($this->getViolationErrorList())
            ->shouldBeLike($this->getExpectedSerializedErrors());
    }

    /**
     * @return ErrorCollection
     */
    public function getViolationErrorList(): ErrorCollection
    {
        return (new ErrorCollection())
            ->setName('deal')
            ->setIsErrorRootElement(true)
            ->setIsEntityField(true)
            ->addErrorCollection(
                (new ErrorCollection())
                    ->setName('type')
                    ->addFieldErrorMessage('Type is required field')
                    ->addFieldErrorMessage('Invalid type')
            )
            ->addErrorCollection(
                (new ErrorCollection())
                    ->setName('dealer')
                    ->setIsEntityField(true)
                    ->addErrorCollection(
                        (new ErrorCollection())
                            ->setName('name')
                            ->addFieldErrorMessage('Dealer name is required field')
                    )
            )
            ->addErrorCollection(
                (new ErrorCollection())
                    ->setName('applications')
                    ->setIsCollectionField(true)
                    ->addErrorCollection(
                        (new ErrorCollection())
                            ->setName('applications')
                            ->setCollectionFieldIndex(0)
                            ->addErrorCollection(
                                (new ErrorCollection())
                                    ->setName('applicants')
                                    ->setIsCollectionField(true)
                                    ->addErrorCollection(
                                        (new ErrorCollection())
                                            ->setName('applicants')
                                            ->setCollectionFieldIndex(0)
                                            ->addErrorCollection(
                                                (new ErrorCollection())
                                                    ->setName('type')
                                                    ->addFieldErrorMessage('Applicant type is required field')
                                                    ->addFieldErrorMessage('Invalid applicant type')
                                            )
                                            ->addErrorCollection(
                                                (new ErrorCollection())
                                                    ->setName('workNumber')
                                                    ->addFieldErrorMessage('Work number is required field')
                                                    ->addFieldErrorMessage('Invalid work number')
                                            )
                                            ->addErrorCollection(
                                                (new ErrorCollection())
                                                    ->setName('bank')
                                                    ->setIsEntityField(true)
                                                    ->addFieldErrorMessage('You need to fill-in bank section to be able to send to this lender')
                                            )
                                    )
                                    ->addErrorCollection(
                                        (new ErrorCollection())
                                            ->setName('applicants')
                                            ->setCollectionFieldIndex(1)
                                            ->addErrorCollection(
                                                (new ErrorCollection())
                                                    ->setName('type')
                                                    ->addFieldErrorMessage('Invalid applicant type')
                                            )
                                            ->addErrorCollection(
                                                (new ErrorCollection())
                                                    ->setName('homeNumber')
                                                    ->addFieldErrorMessage('Invalid home number')
                                            )
                                            ->addErrorCollection(
                                                (new ErrorCollection())
                                                    ->setName('marketing')
                                                    ->setIsEntityField(true)
                                                    ->addErrorCollection(
                                                        (new ErrorCollection())
                                                            ->setName('sms')
                                                            ->addFieldErrorMessage('Invalid sms marketing status')
                                                    )
                                                    ->addErrorCollection(
                                                        (new ErrorCollection())
                                                            ->setName('email')
                                                            ->addFieldErrorMessage('Invalid email marketing status')
                                                    )
                                            )
                                    )
                            )
                            ->addErrorCollection(
                                (new ErrorCollection())
                                    ->setName('financials')
                                    ->setIsEntityField(true)
                                    ->addErrorCollection(
                                        (new ErrorCollection())
                                            ->setName('loanToValue')
                                            ->addFieldErrorMessage('Loan to value is required field')
                                    )
                            )
                            ->addErrorCollection(
                                (new ErrorCollection())
                                    ->setName('asset')
                                    ->setIsEntityField(true)
                                    ->addErrorCollection(
                                        (new ErrorCollection())
                                            ->setName('type')
                                            ->addFieldErrorMessage('Type is required field')
                                    )
                            )
                    )
                    ->addErrorCollection(
                        (new ErrorCollection())
                            ->setName('applications')
                            ->setCollectionFieldIndex(1)
                            ->addErrorCollection(
                                (new ErrorCollection())
                                    ->setName('applicants')
                                    ->setIsCollectionField(true)
                                    ->addErrorCollection(
                                        (new ErrorCollection())
                                            ->setName('applicants')
                                            ->setCollectionFieldIndex(0)
                                            ->addErrorCollection(
                                                (new ErrorCollection())
                                                    ->setName('type')
                                                    ->addFieldErrorMessage('Invalid applicant type')
                                            )
                                    )
                            )
                            ->addErrorCollection(
                                (new ErrorCollection())
                                    ->setName('financials')
                                    ->setIsEntityField(true)
                                    ->addErrorCollection(
                                        (new ErrorCollection())
                                            ->setName('loanToValue')
                                            ->addFieldErrorMessage('Loan to value is required field')
                                    )
                            )
                            ->addErrorCollection(
                                (new ErrorCollection())
                                    ->setName('asset')
                                    ->setIsEntityField(true)
                                    ->addErrorCollection(
                                        (new ErrorCollection())
                                            ->setName('type')
                                            ->addFieldErrorMessage('Type is required field')
                                    )
                            )
                    )
            );
    }

    /**
     * @return array
     */
    public function getExpectedSerializedErrors(): array
    {
        return [
            'fields' =>[
                'type' => [
                    'Type is required field',
                    'Invalid type',
                ],
                'dealer' => [
                    'fields' => [
                        'name' => [
                            'Dealer name is required field'
                        ],
                    ],
                    'errors' => []
                ],
                'applications' => [
                    'items' => [
                        [
                            'fields' => [
                                'applicants' => [
                                    'items' => [
                                        [
                                            'fields' => [
                                                'type' => [
                                                    'Applicant type is required field',
                                                    'Invalid applicant type',
                                                ],
                                                'workNumber' => [
                                                    'Work number is required field',
                                                    'Invalid work number',
                                                ],
                                                'bank' => [
                                                    'fields' =>[],
                                                    'errors' => [
                                                        'You need to fill-in bank section to be able to send to this lender'
                                                    ]
                                                ],
                                            ],
                                            'listItemOrder' => 0,
                                            'errors' => []
                                        ],
                                        [
                                            'fields' => [
                                                'type' => [
                                                    'Invalid applicant type',
                                                ],
                                                'homeNumber' => [
                                                    'Invalid home number',
                                                ],
                                                'marketing' => [
                                                    'fields' =>[
                                                        'sms' => [
                                                            'Invalid sms marketing status'
                                                        ],
                                                        'email' => [
                                                            'Invalid email marketing status'
                                                        ]
                                                    ],
                                                    'errors' => []
                                                ],
                                            ],
                                            'listItemOrder' => 1,
                                            'errors' => []
                                        ]
                                    ],
                                    'errors' => []
                                ],
                                'financials' => [
                                    'fields' => [
                                        'loanToValue' => [
                                            'Loan to value is required field'
                                        ],
                                    ],
                                    'errors' => []
                                ],
                                'asset' => [
                                    'fields' => [
                                        'type' => [
                                            'Type is required field'
                                        ],
                                    ],
                                    'errors' => []
                                ],
                            ],
                            'listItemOrder' => 0,
                            'errors' => []
                        ],
                        [
                            'fields' => [
                                'applicants' => [
                                    'items' => [
                                        [
                                            'fields' => [
                                                'type' => [
                                                    'Invalid applicant type',
                                                ],
                                            ],
                                            'listItemOrder' => 0,
                                            'errors' => []
                                        ],
                                    ],
                                    'errors' => []
                                ],
                                'financials' => [
                                    'fields' => [
                                        'loanToValue' => [
                                            'Loan to value is required field'
                                        ],
                                    ],
                                    'errors' => []
                                ],
                                'asset' => [
                                    'fields' => [
                                        'type' => [
                                            'Type is required field'
                                        ],
                                    ],
                                    'errors' => []
                                ],
                            ],
                            'listItemOrder' => 1,
                            'errors' => []
                        ]
                    ],
                    'errors' => []
                ]
            ],
            'errors' =>[]
        ];
    }
}
