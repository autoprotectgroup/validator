<?php

declare(strict_types=1);

namespace spec\DealTrak\Validator\Validation\ExpressionFunction\Enum;

use DealTrak\Validator\Validation\ExpressionFunction\Enum\EnumIn;
use PhpSpec\ObjectBehavior;
use spec\DealTrak\Validator\Validation\fixtures\validationExpressions\enums\EnumBackedString;
use spec\DealTrak\Validator\Validation\fixtures\validationExpressions\enums\EnumSimpleUnit;

class EnumInSpec extends ObjectBehavior
{
    function it_is_initialisable(): void
    {
        $this->shouldHaveType(EnumIn::class);
    }

    function it_can_validate_enum_with_several_values(): void
    {
        $this->getEvaluator()([], EnumSimpleUnit::NAME_ONE, ['NAME_ONE'])->shouldBe(true);
        $this->getEvaluator()([], EnumSimpleUnit::NAME_TWO, ['NAME_ONE', 'NAME_TWO'])->shouldBe(true);
        $this->getEvaluator()([], EnumSimpleUnit::NAME_TWO, ['NAME_ONE'])->shouldBe(false);

        $this->getEvaluator()([], EnumBackedString::NAME_TWO, ['Name two', 'Name one'])->shouldBe(true);
        $this->getEvaluator()([], EnumBackedString::NAME_TWO, ['Name one'])->shouldBe(false);

        $this->getEvaluator()([], null, ['Name one'])->shouldBe(false);
    }
}
