<?php

declare(strict_types=1);

namespace spec\DealTrak\Validator\Validation\ExpressionFunction\Enum;

use DealTrak\Validator\Validation\ExpressionFunction\Enum\EnumEquals;
use PhpSpec\ObjectBehavior;
use spec\DealTrak\Validator\Validation\fixtures\validationExpressions\enums\EnumBackedInt;
use spec\DealTrak\Validator\Validation\fixtures\validationExpressions\enums\EnumBackedString;
use spec\DealTrak\Validator\Validation\fixtures\validationExpressions\enums\EnumSimpleUnit;

class EnumEqualsSpec extends ObjectBehavior
{
    function it_is_initialisable(): void
    {
        $this->shouldHaveType(EnumEquals::class);
    }

    function it_can_validate_unit_enumeration(): void
    {
        $this->getEvaluator()([], EnumSimpleUnit::NAME_ONE, 'NAME_ONE')->shouldBe(true);
        $this->getEvaluator()([], EnumSimpleUnit::NAME_ONE, 'NOT_EXISTENT')->shouldBe(false);
        $this->getEvaluator()([], EnumSimpleUnit::NAME_ONE, 'NAME_TWO')->shouldBe(false);
    }

    function it_can_validate_null(): void
    {
        $this->getEvaluator()([], null, 'NAME_ONE')->shouldBe(false);
    }

    function it_can_validate_backed_enums(): void
    {
        $this->getEvaluator()([], EnumBackedString::NAME_ONE, 'Name one')->shouldBe(true);
        $this->getEvaluator()([], EnumBackedString::NAME_ONE, 'Non existent')->shouldBe(false);
        $this->getEvaluator()([], EnumBackedString::NAME_ONE, 'Name two')->shouldBe(false);

        $this->getEvaluator()([], EnumBackedInt::NAME_ONE, 1)->shouldBe(true);
        $this->getEvaluator()([], EnumBackedString::NAME_ONE, 33)->shouldBe(false);
        $this->getEvaluator()([], EnumBackedString::NAME_ONE, 2)->shouldBe(false);
    }
}
