# Validator project

Provide validation for the fields given to the validator

## Installation

Create file `auth.json` with your composer credentials from gitlab.com with the contents like:

```json
{"gitlab-token":{"gitlab.com":"your_token"}}
```

Run `docker build -t validator .` to build a docker image.

Then run `docker run  -it --rm -v $(pwd):/application validator composer install` to install dependencies.

## For CI configuration

Run image build with command `docker build --build-arg COMPOSER_AUTH='{"gitlab-token":{"gitlab.com":"your_token"}}' -t validator .`

## Run tests

Unit tests:

```bash
docker run -it --rm -w /application -v $(pwd):/application validator vendor/bin/phpspec run
```

Optional run the tests with debugger just in case:

```bash
docker run -it --rm -e XDEBUG_START_WITH_REQUEST=1 -e IDE_KEY="validation" -e XDEBUG_CLIENT_HOST=docker.for.mac.localhost -e XDEBUG_CLIENT_PORT=9005 -e PHP_IDE_CONFIG="serverName=validation"  -w /application -v $(pwd):/application validator vendor/bin/phpspec run
```
Run syntax check:

```bash
docker run --rm -w /application -v $(pwd):/application validator vendor/bin/phpcs --basepath=/application/src --standard=/application/.phpcs.xml --report-full src
```
