FROM composer:2.4.3 as composerDocker

FROM php:8.1.14-cli-buster

WORKDIR /application

ENV DEBIAN_FRONTEND noninteractive

RUN apt update && \
    apt install -y git && \
    apt install -y zip unzip libzip-dev

RUN pecl install \
    xdebug-3.1.2

RUN docker-php-ext-install bcmath && \
    docker-php-ext-configure zip --with-zip && \
    docker-php-ext-install zip && \
    docker-php-ext-enable xdebug

ADD ./docker/xdebug.ini /usr/local/etc/php/conf.d/docker-php-ext-xdebug.ini

COPY --from=composerDocker /usr/bin/composer /usr/bin/composer

RUN /usr/bin/composer --version

COPY . /application

ARG COMPOSER_AUTH

RUN composer install --prefer-dist
